<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionCreateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionDeleteRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionGetByCustomerIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionGetByIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionUpdateMetaDataRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionUpdateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\ConnectionInterface;

class ConnectionController
{
    public function __construct(private ConnectionInterface $connectionService)
    {
    }

    public function createConnection(ConnectionCreateRequest $request)
    {
        return $this->connectionService->createConnection($request);
    }

    public function updateConnection(ConnectionUpdateRequest $request)
    {
        return $this->connectionService->updateConnection($request);
    }

    public function updateConnectionMetaData(ConnectionUpdateMetaDataRequest $request)
    {
        return $this->connectionService->updateConnectionMetaData($request);
    }

    public function deleteConnection(ConnectionDeleteRequest $request)
    {
        return $this->connectionService->deleteConnection($request);
    }

    public function getConnectionByCustomerId(ConnectionGetByCustomerIdRequest $request)
    {
        return $this->connectionService->getConnectionByCustomerId($request);
    }

    public function getConnectionById(ConnectionGetByIdRequest $request)
    {
        return $this->connectionService->getConnectionById($request);
    }
}
