<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\InfoResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\InfoInterface;

class InfoController
{
    public function __construct(private readonly InfoInterface $infoService)
    {
    }

    public function getInfo(): InfoResponse
    {
        return $this->infoService->getInfo();
    }
}
