<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountGetByConnectionIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountGetByCustomerIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountUpdateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\AccountGetByConnectionIdResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\AccountGetByCustomerIdResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\AccountUpdateResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\AccountInterface;

class AccountController
{
    public function __construct(private readonly AccountInterface $accountService)
    {
    }

    /**
     * @param AccountUpdateRequest $request
     * @return AccountUpdateResponse
     */
    public function updateAccount(AccountUpdateRequest $request): AccountUpdateResponse
    {
        return $this->accountService->updateAccount($request);
    }

    /**
     * @param AccountGetByCustomerIdRequest $request
     * @return AccountGetByCustomerIdResponse
     */
    public function getAccountByCustomerId(AccountGetByCustomerIdRequest $request): AccountGetByCustomerIdResponse
    {
        return $this->accountService->getAccountByCustomerId($request);
    }

    /**
     * @param AccountGetByConnectionIdRequest $request
     * @return AccountGetByConnectionIdResponse
     */
    public function getAccountByConnectionId(AccountGetByConnectionIdRequest $request): AccountGetByConnectionIdResponse
    {
        return $this->accountService->getAccountByConnectionId($request);
    }
}
