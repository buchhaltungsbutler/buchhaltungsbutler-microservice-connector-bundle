<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\SyncCustomerRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\SyncInterface;

class SyncController
{
    public function __construct(
        private SyncInterface $syncService,
    )
    {
    }

    public function syncCustomer(SyncCustomerRequest $request)
    {
        return $this->syncService->syncCustomer($request);
    }
}
