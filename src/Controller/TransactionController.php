<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\TransactionListRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\TransactionListResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Service\TransactionService;

class TransactionController
{
    public function __construct(private TransactionService $transactionService)
    {
    }

    public function getTransactionsByAccountId(TransactionListRequest $request)
    {

        $transactions = $this->transactionService->getTransactionsByAccountId($request->getAccountId(), $request->getLastTransactionId(), $request->getLimit());

        return new TransactionListResponse($transactions);
    }
}
