<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\OAuthGetUrlRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\OAuthInterface;

class OAuthController
{
    public function __construct(private OAuthInterface $oAuthService)
    {
    }

    public function getOAuthUrl(OAuthGetUrlRequest $request)
    {
        return $this->oAuthService->getOAuthUrl($request);
    }
}
