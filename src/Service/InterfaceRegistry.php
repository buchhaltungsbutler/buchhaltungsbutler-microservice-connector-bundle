<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Service;

/**
 * @codeCoverageIgnore
 */
class InterfaceRegistry
{
    public function __construct(private \IteratorAggregate $interfaces)
    {
    }

    public function getAllInterfaces(): array
    {
        return iterator_to_array($this->interfaces->getIterator());
    }
}
