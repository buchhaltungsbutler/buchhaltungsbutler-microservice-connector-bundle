<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Service;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class BHBResponseToSymfonyResponseConverter
{
    public function convert(Response $response): \Symfony\Component\HttpFoundation\Response
    {
        return new JsonResponse($response, $response->getStatusCode());
    }
}
