<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Service;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestArgumentValueResolverException\CouldNotParseRequestException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\RequestParser;
use IteratorAggregate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class RequestArgumentValueResolver implements ArgumentValueResolverInterface
{

    /**
     * @var RequestParser[] $requestParsers
     */
    private array $requestParsers;
    public function __construct(IteratorAggregate $requestParsers)
    {
        $this->requestParsers = iterator_to_array($requestParsers);
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return is_subclass_of($argument->getType(), \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request::class);
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return iterable
     * @throws CouldNotParseRequestException
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        foreach ($this->requestParsers as $requestParser) {
            if ($requestParser->supports($argument->getType())) {
                yield $requestParser->parse($request);
                return; // only one parser can parse the request
            }
        }

        throw new CouldNotParseRequestException("no valid parser found for request with type: ". $argument->getType(), []);
    }
}
