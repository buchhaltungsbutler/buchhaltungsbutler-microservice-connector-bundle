<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Service\EventSubscriber;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\BaseExceptionInterface;
use BuchhaltungsButler\MicroserviceConnectorBundle\Service\BHBResponseToSymfonyResponseConverter;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\RequestIdService;
use BuchhaltungsButler\MicroserviceConnectorBundle\Service\ThrowableToResponseConverter;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ErrorEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly ThrowableToResponseConverter          $throwableToResponseConverter,
        private readonly BHBResponseToSymfonyResponseConverter $responseConverter,
        private readonly RequestIdService                      $requestIdService,
        private readonly LoggerInterface                       $logger
    )
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['onKernelException', 0],
            ],
            ConsoleEvents::ERROR => [
                ['onConsoleException', 0],
            ],
        ];
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $throwable = $event->getThrowable();

        $this->log($throwable);

        $event->setResponse(
            $this->responseConverter->convert(
                $this->throwableToResponseConverter->getResponseForThrowable(
                    $this->requestIdService->getRequestId(),
                    $throwable
                )
            )
        );
    }

    public function onConsoleException(ConsoleErrorEvent $event)
    {
        // todo fix logging
        $throwable = $event->getError();

        $this->log($throwable);

        $event->setExitCode(1);
    }

    /**
     * log exceptions
     *
     * @param \Throwable $throwable
     * @param string $logFunction
     */
    private function log(\Throwable $throwable, string $logFunction = 'error')
    {
        $message = $throwable->getMessage();
        $context['trace'] = $throwable->getTraceAsString();

        $previous = $throwable->getPrevious();
        if (!is_null($previous)) {
            $context['previous'] = [
                'message'   => $previous->getMessage(),
                'exception' => get_class($previous),
                'code'      => $previous->getCode(),
                'file'      => $previous->getFile(),
                'line'      => $previous->getLine(),
                'trace'     => $previous->getTraceAsString(),
            ];
        }

        if ($throwable instanceof BaseExceptionInterface) {
            $context['data'] = $throwable->getData();
        }
        $this->logger->$logFunction($message, $context);
    }
}
