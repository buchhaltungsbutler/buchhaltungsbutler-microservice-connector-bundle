<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Service\EventSubscriber;

use BuchhaltungsButler\MicroserviceConnectorBundle\Service\BHBResponseToSymfonyResponseConverter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ResponseEventSubscriber implements EventSubscriberInterface
{
    public function __construct(private BHBResponseToSymfonyResponseConverter $responseConverter)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => [
                ['onKernelView', 0],
            ],
        ];
    }

    public function onKernelView(ViewEvent $event)
    {
        $result = $event->getControllerResult();

        if (!$result instanceof \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\Response) {
            return;
        }

        $event->setResponse($this->responseConverter->convert($result));
    }
}
