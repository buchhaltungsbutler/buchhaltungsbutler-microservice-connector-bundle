<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Service;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\ErrorResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\Response;

class ThrowableToResponseConverter
{
    public function getResponseForThrowable(string $requestId, \Throwable $throwable): Response
    {
        if ($throwable instanceof \RuntimeException) {
            return new ErrorResponse(
                $requestId,
                ' Runtime Exception: ' . $throwable->getMessage(),
                $throwable->getCode()
            );
        }

        return new ErrorResponse(
            $requestId,
            $throwable->getMessage(),
            $throwable->getCode()
        );
    }
}
