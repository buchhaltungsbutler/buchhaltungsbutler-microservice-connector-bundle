<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Service;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Account;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Transaction;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\AccountNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\AccountRepository;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\TransactionRepository;

class TransactionService
{
    public function __construct(
        private TransactionRepository $transactionRepository,
        private AccountRepository $accountRepository
    )
    {
    }

    /**
     * @param int $accountId
     * @param int|null $lastTransactionId
     * @param int|null $limit
     * @return Transaction[]
     * @throws \Exception
     */
    public function getTransactionsByAccountId(int $accountId, ?int $lastTransactionId = null, ?int $limit = null): array
    {
        $account = $this->accountRepository->find($accountId);

        if (!$account instanceof Account) {
            throw new AccountNotFoundException('account not found with id ' . $accountId);
        }

        return $this->transactionRepository->getTransactionsByAccount(
            $account,
            $lastTransactionId,
            $limit
        );
    }
}
