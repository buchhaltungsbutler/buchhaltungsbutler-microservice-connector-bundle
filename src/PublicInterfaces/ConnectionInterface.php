<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\ConnectionNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionCreateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionDeleteRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionGetByCustomerIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionGetByIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionUpdateMetaDataRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionUpdateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\ConnectionCreateResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\ConnectionDeleteResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\ConnectionGetByCustomerIdResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\ConnectionGetByIdResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\ConnectionUpdateMetaDataResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\ConnectionUpdateResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\ConnectionPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\CustomerPersister;

abstract class ConnectionInterface extends BaseInterface
{
    abstract public function createConnection(ConnectionCreateRequest $request): ConnectionCreateResponse;
    abstract public function updateConnection(ConnectionUpdateRequest $request): ConnectionUpdateResponse;
    abstract public function deleteConnection(ConnectionDeleteRequest $request): ConnectionDeleteResponse;

    /**
     * @param ConnectionUpdateMetaDataRequest $request
     * @return ConnectionUpdateMetaDataResponse
     * @throws ConnectionNotFoundException
     */
    public function updateConnectionMetaData(ConnectionUpdateMetaDataRequest $request): ConnectionUpdateMetaDataResponse
    {
        $connectionEntity = $this->connectionPersister->getConnectionById($request->getConnectionId());

        if (is_null($connectionEntity)) {
            throw new ConnectionNotFoundException('connection not found for id: '. $request->getConnectionId());
        }

        $connectionEntity->setMetaData($request->getMetadata());

        $this->connectionPersister->persist($connectionEntity);

        return new ConnectionUpdateMetaDataResponse($connectionEntity->getId());
    }

    /**
     * @param ConnectionPersister $connectionPersister
     * @param CustomerPersister $customerPersister
     */
    public function __construct(
        protected ConnectionPersister $connectionPersister,
        protected CustomerPersister $customerPersister,
    )
    {
    }

    /**
     * @param ConnectionGetByCustomerIdRequest $request
     * @return ConnectionGetByCustomerIdResponse
     * @throws \BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException
     */
    public function getConnectionByCustomerId(ConnectionGetByCustomerIdRequest $request): ConnectionGetByCustomerIdResponse
    {
        $connections = $this->connectionPersister->getConnectionsByCustomerId(
            $request->getCustomerId(),
            $request->getLimit()
        );

        return new ConnectionGetByCustomerIdResponse($connections);
    }

    /**
     * @param ConnectionGetByIdRequest $request
     * @return ConnectionGetByIdResponse
     */
    public function getConnectionById(ConnectionGetByIdRequest $request): ConnectionGetByIdResponse
    {
        $connection = $this->connectionPersister->getConnectionById($request->getConnectionId());

        return new ConnectionGetByIdResponse($connection);
    }
}
