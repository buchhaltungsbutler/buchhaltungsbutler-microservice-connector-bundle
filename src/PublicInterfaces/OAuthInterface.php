<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces;


use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\OAuthGetUrlRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\OAuthGetUrlResponse;

abstract class OAuthInterface extends BaseInterface
{
    abstract public function getOAuthUrl(OAuthGetUrlRequest $request): OAuthGetUrlResponse;
}
