<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\SyncCustomerRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\SyncCustomerResponse;

abstract class SyncInterface extends BaseInterface
{
    abstract public function syncCustomer(SyncCustomerRequest $request): SyncCustomerResponse;
}
