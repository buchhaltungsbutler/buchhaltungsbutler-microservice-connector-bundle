<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces;


use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\InfoResponse;

abstract class InfoInterface extends BaseInterface
{
    abstract public function getInfo(): InfoResponse;
}
