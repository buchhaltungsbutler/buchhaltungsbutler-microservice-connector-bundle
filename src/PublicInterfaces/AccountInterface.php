<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces;


use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountGetByConnectionIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountGetByCustomerIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountUpdateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\AccountGetByConnectionIdResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\AccountGetByCustomerIdResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\AccountUpdateResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\AccountPersister;

abstract class AccountInterface extends BaseInterface
{
    abstract public function updateAccount(AccountUpdateRequest $request): AccountUpdateResponse;

    public function __construct(protected AccountPersister $accountPersister)
    {
    }

    public function getAccountByCustomerId(AccountGetByCustomerIdRequest $request): AccountGetByCustomerIdResponse
    {
        $accounts = $this->accountPersister->getAccountsByCustomerId(
            $request->getCustomerId(),
            $request->getLimit()
        );

        return new AccountGetByCustomerIdResponse($accounts);
    }

    public function getAccountByConnectionId(AccountGetByConnectionIdRequest $request): AccountGetByConnectionIdResponse
    {
        $accounts = $this->accountPersister->getAccountsByConnectionId(
            $request->getConnectionId(),
            $request->getLimit()
        );

        return new AccountGetByConnectionIdResponse($accounts);
    }
}
