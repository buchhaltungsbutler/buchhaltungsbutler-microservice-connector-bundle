<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Entity;

use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\TransactionRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TransactionRepository::class)]
#[ORM\Table(name: '`transaction`')]
#[ORM\UniqueConstraint(
    name: 'fk_account_id_source_transaction_id',
    columns: ['fk_account_id', 'source_transaction_id', 'deleted']
)]
class Transaction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'pk', type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity:Account::class)]
    #[ORM\JoinColumn(name:"fk_account_id", referencedColumnName: "pk", nullable: false, onDelete: 'CASCADE')]
    private Account $account;

    #[ORM\Column(name: 'amount', type: 'decimal', precision: 10, scale: 2)]
    private float $amount;

    #[ORM\Column(name: 'currency', type: 'string', length: 3)]
    private string $currency;

    #[ORM\Column(name: 'fee_amount', type: 'decimal', precision: 10, scale: 2)]
    private float $feeAmount;

    #[ORM\Column(name: 'purpose', type: 'text')]
    private string $purpose;

    #[ORM\Column(name: 'payment_reference', type: 'string', length: 254)]
    private string $paymentReference;

    #[ORM\Column(name: 'type', type: 'string', length: 254)]
    private string $type;

    #[ORM\Column(name: 'counter_party', type: 'text')]
    private string $counterParty;

    #[ORM\Column(name: 'booking_text', type: 'text')]
    private string $bookingText;

    #[ORM\Column(name: 'booked_at', type: 'datetime_immutable')]
    private DateTimeImmutable $bookedAt;

    #[ORM\Column(name: 'created_at', type: 'datetime_immutable')]
    private DateTimeImmutable $createdAt;

    #[ORM\Column(name: 'valued_at', type: 'datetime_immutable')]
    private DateTimeImmutable $valuedAt;

    #[ORM\Column(name: 'source_transaction_id', type: 'string', length: 190)]
    private string $sourceTransactionId;

    #[ORM\Column(name: 'deleted', type: 'boolean')]
    private bool $deleted;

    /**
     * @param int $id
     * @param Account $account
     * @param float $amount
     * @param string $currency
     * @param float $feeAmount
     * @param string $purpose
     * @param string $paymentReference
     * @param string $type
     * @param string $counterParty
     * @param string $bookingText
     * @param DateTimeImmutable $bookedAt
     * @param DateTimeImmutable $createdAt
     * @param DateTimeImmutable $valuedAt
     * @param string $sourceTransactionId
     * @param bool $deleted
     */
    public function __construct(
        int $id,
        Account $account,
        float $amount,
        string $currency,
        float $feeAmount,
        string $purpose,
        string $paymentReference,
        string $type,
        string $counterParty,
        string $bookingText,
        DateTimeImmutable $bookedAt,
        DateTimeImmutable $createdAt,
        DateTimeImmutable $valuedAt,
        string $sourceTransactionId,
        bool $deleted
    )
    {
        $this->id = $id;
        $this->account = $account;
        $this->amount = $amount;
        $this->currency = $currency;
        $this->feeAmount = $feeAmount;
        $this->purpose = $purpose;
        $this->paymentReference = $paymentReference;
        $this->type = $type;
        $this->counterParty = $counterParty;
        $this->bookingText = $bookingText;
        $this->bookedAt = $bookedAt;
        $this->createdAt = $createdAt;
        $this->valuedAt = $valuedAt;
        $this->sourceTransactionId = $sourceTransactionId;
        $this->deleted = $deleted;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return float
     */
    public function getFeeAmount(): float
    {
        return $this->feeAmount;
    }

    /**
     * @return string
     */
    public function getPurpose(): string
    {
        return $this->purpose;
    }

    /**
     * @return string
     */
    public function getPaymentReference(): string
    {
        return $this->paymentReference;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getCounterParty(): string
    {
        return $this->counterParty;
    }

    /**
     * @return string
     */
    public function getBookingText(): string
    {
        return $this->bookingText;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getBookedAt(): DateTimeImmutable
    {
        return $this->bookedAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getValuedAt(): DateTimeImmutable
    {
        return $this->valuedAt;
    }

    /**
     * @return string
     */
    public function getSourceTransactionId(): string
    {
        return $this->sourceTransactionId;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted): void
    {
        $this->deleted = $deleted;
    }
}
