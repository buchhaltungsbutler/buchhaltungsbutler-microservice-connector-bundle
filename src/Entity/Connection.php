<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Entity;

use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\ConnectionRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ConnectionRepository::class)]
#[ORM\Table(name: '`connection`')]
class Connection implements MetaDataEntityInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'pk', type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity:Customer::class)]
    #[ORM\JoinColumn(name:"fk_customer_id", referencedColumnName: "pk", nullable: false, onDelete: 'CASCADE')]
    private Customer $customer;

    #[ORM\Column(name: 'description', type: 'string')]
    private string $description;

    #[ORM\Column(name: 'production', type: 'boolean')]
    private bool $production;

    #[ORM\Column(name: 'market', type: 'string', length: 3)]
    private string $market;

    #[ORM\Column(name: 'broken', type: 'boolean')]
    private bool $broken;

    #[ORM\Column(name: 'meta_data', type: 'json', nullable: true)]
    private ?array $metaData;

    #[ORM\Column(name: 'access_token', type: 'string', length: 4096)]
    private string $accessToken;

    #[ORM\Column(name: 'access_expire_time', type: 'datetime_immutable')]
    private DateTimeImmutable $accessExpireTime;

    #[ORM\Column(name: 'refresh_token', type: 'string', length: 4096)]
    private string $refreshToken;

    #[ORM\Column(name: 'refresh_expire_time', type: 'datetime_immutable')]
    private DateTimeImmutable $refreshExpireTime;

    #[ORM\Column(name: 'updated_at', type: 'datetime_immutable')]
    private DateTimeImmutable $updatedAt;

    #[ORM\Column(name: 'created_at', type: 'datetime_immutable')]
    private DateTimeImmutable $createdAt;

    /**
     * @param int $id
     * @param Customer $customer
     * @param string $description
     * @param bool $production
     * @param string $market
     * @param bool $broken
     * @param array|null $metaData
     * @param string $accessToken
     * @param DateTimeImmutable $accessExpireTime
     * @param string $refreshToken
     * @param DateTimeImmutable $refreshExpireTime
     * @param DateTimeImmutable $updatedAt
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(
        int $id,
        Customer $customer,
        string $description,
        bool $production,
        string $market,
        bool $broken,
        ?array $metaData,
        string $accessToken,
        DateTimeImmutable $accessExpireTime,
        string $refreshToken,
        DateTimeImmutable $refreshExpireTime,
        DateTimeImmutable $updatedAt,
        DateTimeImmutable $createdAt
    )
    {
        $this->id = $id;
        $this->customer = $customer;
        $this->description = $description;
        $this->production = $production;
        $this->market = $market;
        $this->broken = $broken;
        $this->metaData = $metaData;
        $this->accessToken = $accessToken;
        $this->accessExpireTime = $accessExpireTime;
        $this->refreshToken = $refreshToken;
        $this->refreshExpireTime = $refreshExpireTime;
        $this->updatedAt = $updatedAt;
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isProduction(): bool
    {
        return $this->production;
    }

    /**
     * @param bool $production
     */
    public function setProduction(bool $production): void
    {
        $this->production = $production;
    }

    /**
     * @return string
     */
    public function getMarket(): string
    {
        return $this->market;
    }

    /**
     * @param string $market
     */
    public function setMarket(string $market): void
    {
        $this->market = $market;
    }

    /**
     * @return bool
     */
    public function isBroken(): bool
    {
        return $this->broken;
    }

    /**
     * @param bool $broken
     */
    public function setBroken(bool $broken): void
    {
        $this->broken = $broken;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getAccessExpireTime(): DateTimeImmutable
    {
        return $this->accessExpireTime;
    }

    /**
     * @param DateTimeImmutable $accessExpireTime
     */
    public function setAccessExpireTime(DateTimeImmutable $accessExpireTime): void
    {
        $this->accessExpireTime = $accessExpireTime;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken(string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getRefreshExpireTime(): DateTimeImmutable
    {
        return $this->refreshExpireTime;
    }

    /**
     * @param DateTimeImmutable $refreshExpireTime
     */
    public function setRefreshExpireTime(DateTimeImmutable $refreshExpireTime): void
    {
        $this->refreshExpireTime = $refreshExpireTime;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTimeImmutable $updatedAt
     */
    public function setUpdatedAt(DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeImmutable $createdAt
     */
    public function setCreatedAt(DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return array|null
     */
    public function getMetaData(): ?array
    {
        return $this->metaData;
    }

    /**
     * @param array|null $metaData
     */
    public function setMetaData(?array $metaData): void
    {
        $this->metaData = $metaData;
    }
}
