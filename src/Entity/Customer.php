<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Entity;

use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\CustomerRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]
#[ORM\Table(name: '`customer`')]
#[ORM\UniqueConstraint(
    name: 'customer_id',
    columns: ['customer_id']
)]
class Customer implements MetaDataEntityInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'pk', type: 'integer')]
    private int $id;

    #[ORM\Column(name: 'customer_id', type: 'string', length: 190)]
    private string $customerId;

    #[ORM\Column(name: 'created_at', type: 'datetime_immutable')]
    private DateTimeImmutable $createdAt;

    #[ORM\Column(name: 'meta_data', type: 'json', nullable: true)]
    private ?array $metaData;

    /**
     * @param int $id
     * @param string $customerId
     * @param DateTimeImmutable $createdAt
     * @param array|null $metaData
     */
    public function __construct(
        int $id,
        string $customerId,
        DateTimeImmutable $createdAt,
        ?array $metaData = null
    )
    {
        $this->id = $id;
        $this->customerId = $customerId;
        $this->createdAt = $createdAt;
        $this->metaData = $metaData;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return array|null
     */
    public function getMetaData(): ?array
    {
        return $this->metaData;
    }

    /**
     * @param array|null $metaData
     */
    public function setMetaData(?array $metaData): void
    {
        $this->metaData = $metaData;
    }

}
