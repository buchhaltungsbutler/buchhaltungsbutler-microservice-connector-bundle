<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Entity;

interface MetaDataEntityInterface {

    /**
     * @return array|null
     */
    public function getMetaData(): ?array;

    /**
     * @param array|null $metaData
     */
    public function setMetaData(?array $metaData): void;
}