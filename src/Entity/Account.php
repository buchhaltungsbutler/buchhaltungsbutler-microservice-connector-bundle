<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Entity;

use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\AccountRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AccountRepository::class)]
#[ORM\Table(name: '`account`')]
class Account implements MetaDataEntityInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'pk', type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity:Connection::class)]
    #[ORM\JoinColumn(name:"fk_connection_id", referencedColumnName: "pk", nullable: false, onDelete: 'CASCADE')]
    private Connection $connection;

    #[ORM\Column(name: 'description', type: 'string', length: 254)]
    private string $description;

    #[ORM\Column(name: 'meta_data', type: 'json', nullable: true)]
    private ?array $metaData;

    #[ORM\Column(name: 'import_since', type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $importSince;

    #[ORM\Column(name: 'last_sync', type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $lastSync;

    #[ORM\Column(name: 'last_sync_successful', type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $lastSyncSuccessful;

    #[ORM\Column(name: 'updated_at', type: 'datetime_immutable')]
    private DateTimeImmutable $updatedAt;

    #[ORM\Column(name: 'created_at', type: 'datetime_immutable')]
    private DateTimeImmutable $createdAt;

    /**
     * @param int $id
     * @param Connection $connection
     * @param string $description
     * @param array|null $metaData
     * @param DateTimeImmutable|null $importSince
     * @param DateTimeImmutable|null $lastSync
     * @param DateTimeImmutable|null $lastSyncSuccessful
     * @param DateTimeImmutable $updatedAt
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(
        int $id,
        Connection $connection,
        string $description,
        ?array $metaData,
        ?DateTimeImmutable $importSince,
        ?DateTimeImmutable $lastSync,
        ?DateTimeImmutable $lastSyncSuccessful,
        DateTimeImmutable $updatedAt,
        DateTimeImmutable $createdAt
    )
    {
        $this->id = $id;
        $this->connection = $connection;
        $this->description = $description;
        $this->metaData = $metaData;
        $this->importSince = $importSince;
        $this->lastSync = $lastSync;
        $this->lastSyncSuccessful = $lastSyncSuccessful;
        $this->updatedAt = $updatedAt;
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->connection;
    }

    /**
     * @param Connection $connection
     */
    public function setConnection(Connection $connection): void
    {
        $this->connection = $connection;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getImportSince(): ?DateTimeImmutable
    {
        return $this->importSince;
    }

    /**
     * @param DateTimeImmutable|null $importSince
     */
    public function setImportSince(?DateTimeImmutable $importSince): void
    {
        $this->importSince = $importSince;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getLastSync(): ?DateTimeImmutable
    {
        return $this->lastSync;
    }

    /**
     * @param DateTimeImmutable|null $lastSync
     */
    public function setLastSync(?DateTimeImmutable $lastSync): void
    {
        $this->lastSync = $lastSync;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getLastSyncSuccessful(): ?DateTimeImmutable
    {
        return $this->lastSyncSuccessful;
    }

    /**
     * @param DateTimeImmutable|null $lastSyncSuccessful
     */
    public function setLastSyncSuccessful(?DateTimeImmutable $lastSyncSuccessful): void
    {
        $this->lastSyncSuccessful = $lastSyncSuccessful;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTimeImmutable $updatedAt
     */
    public function setUpdatedAt(DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeImmutable $createdAt
     */
    public function setCreatedAt(DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return array|null
     */
    public function getMetaData(): ?array
    {
        return $this->metaData;
    }

    /**
     * @param array|null $metaData
     */
    public function setMetaData(?array $metaData): void
    {
        $this->metaData = $metaData;
    }
}
