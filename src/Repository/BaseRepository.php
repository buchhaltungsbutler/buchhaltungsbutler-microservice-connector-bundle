<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Repository;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\MetaDataEntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @codeCoverageIgnore
 *
 * @template T of object
 * @template-extends ServiceEntityRepository<T>
 *
 * @method object|null find($id, $lockMode = null, $lockVersion = null)
 * @method object|null findOneBy(array $criteria, array $orderBy = null)
 * @method object[]    findAll()
 * @method object[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
abstract class BaseRepository extends ServiceEntityRepository
{
    private readonly ManagerRegistry $managerRegistry;

    /**
     * @param ManagerRegistry $registry
     * @param string $entityClass
     * @psalm-param class-string<T> $entityClass
     */
    public function __construct(ManagerRegistry $registry, string $entityClass)
    {
        parent::__construct($registry, $entityClass);
        $this->managerRegistry = $registry;
    }

    /**
     * @return bool
     */
    public function isOpen(): bool
    {
        return $this->_em->isOpen();
    }

    /**
     * @return void
     */
    public function reopen()
    {
        if (!$this->_em->isOpen()) {
            $this->managerRegistry->resetManager();
            $this->managerRegistry->resetManager('microservice_connector_bundle');
        }
    }

    /**
     * @return void
     */
    public function flush()
    {
        $this->_em->flush();
    }

    /**
     * @return void
     */
    public function clear()
    {
        $this->_em->clear();
    }

    /**
     * @return void
     */
    public function refresh(object $entity)
    {
        $this->_em->refresh($entity);
    }

    /**
     * @param object $entity
     * @psalm-param T $entity
     * @param bool $flush
     */
    public function persist(object $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param object $entity
     * @psalm-param T $entity
     * @param bool $flush
     */
    public function remove(object $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function beginTransaction()
    {
        $this->_em->beginTransaction();
    }

    public function commit()
    {
        $this->_em->commit();
    }

    public function rollback()
    {
        $this->_em->rollback();
    }

    /**
     * @template TM of MetaDataEntityInterface
     * @param TM $entity
     * @param string $key
     * @param $data
     * @param bool $flush
     * @return void
     * @throws \Exception
     */
    public function updateMetaData(MetaDataEntityInterface $entity, string $key, $data, bool $flush = true)
    {
        $metaData = $entity->getMetaData();
        $metaData[$key] = $data;
        $entity->setMetaData($metaData);
        $this->persist($entity, $flush);
    }
}
