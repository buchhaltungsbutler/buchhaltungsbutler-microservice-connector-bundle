<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Repository;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Account;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Transaction;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends BaseRepository<Transaction>
 *
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    /**
     * @param Account $account
     * @param int|null $lastTransactionId
     * @param int|null $limit
     * @return Transaction[]
     */
    public function getTransactionsByAccount(Account $account, ?int $lastTransactionId = null, ?int $limit = null): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('account', $account));
        $criteria->andWhere(Criteria::expr()->eq('deleted', false));
        if (is_int($lastTransactionId)) { $criteria->andWhere(Criteria::expr()->gt('id', $lastTransactionId)); }
        if (is_int($limit)) { $criteria->setMaxResults($limit); }
        $criteria->orderBy(['id' => 'ASC']);

        return $this->matching($criteria)->toArray();

    }
}
