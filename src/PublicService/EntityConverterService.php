<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicService;

use ReflectionClass;
use ReflectionException;

class EntityConverterService
{
    /**
     * @param string $entityClass
     * @param array $data
     * @return mixed
     * @throws ReflectionException
     */
    public static function fromArray(string $entityClass, array $data): mixed
    {
        $reflection = new ReflectionClass($entityClass);

        $constructorParameters = [];
        foreach ($reflection->getConstructor()->getParameters() as $parameter) {
            $propertyName = $parameter->getName();
            $constructorParameters[$propertyName] = $data[$propertyName] ?? null;
        }

        return new ($entityClass)(...$constructorParameters);
    }

    /**
     * @param object $entity
     * @return array
     * @throws ReflectionException
     */
    public static function toArray(object $entity): array
    {
        $data = [];
        $reflection = new ReflectionClass($entity);

        foreach ($reflection->getProperties() as $property) {
            /** @phpstan-ignore-next-line */
            if ($property->getType()->getName() === 'bool') {
                $getterFunctionPrefix = 'is';
            } else {
                $getterFunctionPrefix = 'get';
            }
            $propertyName = $property->getName();
            $functionName = $getterFunctionPrefix. strtoupper($propertyName[0]).substr($propertyName, 1);
            $data[$propertyName] = $entity->$functionName();
        }

        return $data;
    }

}