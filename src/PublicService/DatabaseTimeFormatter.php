<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicService;


use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;

class DatabaseTimeFormatter
{
    public const DB_TIMEZONE = '+0000';

    /**
     * @param DateTimeInterface $dateTime
     * @return string
     * @throws Exception
     */
    public static function dateTimeToString(DateTimeInterface $dateTime): string
    {
        $dateTimeOut = new DateTimeImmutable($dateTime->format(DATE_ATOM), new DateTimeZone(self::DB_TIMEZONE));
        return $dateTimeOut->format('Y-m-d H:i:s');
    }

    /**
     * @param string $dateTimeString
     * @return DateTimeImmutable
     * @throws Exception
     */
    public static function stringToDateTimeImmutable(string $dateTimeString): DateTimeImmutable
    {
        return new DateTimeImmutable($dateTimeString, new DateTimeZone(self::DB_TIMEZONE));
    }
}
