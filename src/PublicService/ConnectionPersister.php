<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicService;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Connection;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Customer;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\ConnectionNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\ConnectionRepository;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\CustomerRepository;

class ConnectionPersister extends BasePersister
{
    public function __construct(
        private readonly ConnectionRepository   $connectionRepository,
        private readonly CustomerRepository     $customerRepository
    )
    {
        parent::__construct($this->connectionRepository);
    }

    /**
     * @param int $connectionId
     * @param bool $flush
     * @return void
     * @throws ConnectionNotFoundException
     */
    public function removeById(int $connectionId, bool $flush = true)
    {
        $connection = $this->connectionRepository->find($connectionId);
        if (!$connection instanceof Connection) {
            throw new ConnectionNotFoundException("couldn't find connection with id: " . $connectionId);
        }
        $this->connectionRepository->remove($connection, $flush);
    }

    /**
     * @param int $id
     * @param null $limit
     * @return Connection|null
     */
    public function getConnectionById(int $id, $limit = null): ?Connection
    {
        return $this->connectionRepository->find($id);
    }

    /**
     * @param string $customerId
     * @param null $limit
     * @return array
     * @throws CustomerNotFoundException
     */
    public function getConnectionsByCustomerId(string $customerId, $limit = null): array
    {
        $customer = $this->customerRepository->findOneby(['customerId' => $customerId]);

        if (!$customer instanceof Customer) {
            throw new CustomerNotFoundException("couldn't find customer with customerId: " . $customerId);
        }

        return $this->connectionRepository->findBy(
            ['customer' => $customer],
            null,
            $limit
        );
    }

    /**
     * @param Connection $entity
     * @param string $key
     * @param $data
     * @param bool $flush
     * @return void
     * @throws \Exception
     */
    public function updateMetaData(Connection $entity, string $key, $data, bool $flush = true)
    {
        $this->connectionRepository->updateMetaData($entity, $key, $data, $flush);
    }
}
