<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicService;

class RequestIdService
{
    private string $requestId = '';

    public function __construct(private $microserviceId)
    {
    }

    /**
     * @return string
     */
    public function getRequestId(): string
    {
        if (empty($this->requestId)) {
            $this->requestId = uniqid($this->microserviceId.'_', true);
        }
        return $this->requestId;
    }

    /**
     * @param string $requestId
     */
    public function setRequestId(string $requestId): void
    {
        $this->requestId = $requestId;
    }

}
