<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicService;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Account;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Connection;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Customer;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\AccountNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\ConnectionNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\AccountRepository;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\ConnectionRepository;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\CustomerRepository;
use Exception;

class AccountPersister extends BasePersister
{
    public function __construct(
        private readonly AccountRepository      $accountRepository,
        private readonly ConnectionRepository   $connectionRepository,
        private readonly CustomerRepository     $customerRepository,
    )
    {
        parent::__construct($this->accountRepository);
    }

    /**
     * @param int $accountId
     * @param bool $flush
     * @return void
     * @throws AccountNotFoundException
     */
    public function removeById(int $accountId, bool $flush = true): void
    {
        $account = $this->accountRepository->find($accountId);
        if (empty($account)) {
            throw new AccountNotFoundException("couldn't find account with id: " . $accountId);
        }
        $this->accountRepository->remove($account, $flush);
    }

    /**
     * @param int $id
     * @return Account|null
     */
    public function getAccountById(int $id): ?Account
    {
        return $this->accountRepository->find($id);
    }

    /**
     * @param int $connectionId
     * @param int|null $limit
     * @return array
     * @throws ConnectionNotFoundException
     */
    public function getAccountsByConnectionId(int $connectionId, ?int $limit = null): array
    {
        $connection = $this->connectionRepository->find($connectionId);
        if (!$connection instanceof Connection) {
            throw new ConnectionNotFoundException("couldn't find connection with id: " . $connectionId);
        }
        return $this->getAccountsByConnection($connection, $limit);
    }

    /**
     * @param Connection $connection
     * @param int|null $limit
     * @return array
     */
    public function getAccountsByConnection(Connection $connection, ?int $limit = null): array
    {
        return $this->accountRepository->findBy(
            ['connection' => $connection],
            null,
            $limit
        );
    }

    /**
     * @param string $customerId
     * @param int|null $limit
     * @return array
     * @throws CustomerNotFoundException
     */
    public function getAccountsByCustomerId(string $customerId, ?int $limit = null): array
    {
        $customer = $this->customerRepository->findOneBy(
            ['customerId' => $customerId]
        );

        if (!$customer instanceof Customer) {
            throw new CustomerNotFoundException("couldn't find customer with id: " . $customerId);
        }

        $connections = $this->connectionRepository->findBy(
            ['customer' => $customer]
        );

        $result = [];

        foreach ($connections as $connection) {
            $accounts = $this->accountRepository->findBy(
                ['connection' => $connection],
                null,
                $limit
            );
            $result = array_merge($result, $accounts);
        }
         return $result;
    }

    /**
     * @param Account $entity
     * @param string $key
     * @param $data
     * @param bool $flush
     * @return void
     * @throws Exception
     */
    public function updateMetaData(Account $entity, string $key, $data, bool $flush = true): void
    {
        $this->accountRepository->updateMetaData($entity, $key, $data, $flush);
    }
}
