<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicService;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Account;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Transaction;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\AccountNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\AccountRepository;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\TransactionRepository;

class TransactionPersister extends BasePersister
{
    public function __construct(
        private readonly TransactionRepository  $transactionRepository,
        private readonly AccountRepository      $accountRepository
    )
    {
        parent::__construct($this->transactionRepository);
    }

    /**
     * @param string $sourceId
     * @param int $accountId
     * @param bool $deleted
     * @return Transaction|null
     * @throws AccountNotFoundException
     */
    public function findBySourceIdAndAccount(string $sourceId, int $accountId, bool $deleted = false): ?Transaction
    {
        $account = $this->accountRepository->find($accountId);
        if (!$account instanceof Account) {
            throw new AccountNotFoundException("couldn't find account with id: " . $accountId);
        }
        return $this->transactionRepository->findOneBy([
            'sourceTransactionId' => $sourceId,
            'account' => $account,
            'deleted' => $deleted,
        ]);
    }
}
