<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicService;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Transaction;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\BaseRepository;

abstract class BasePersister
{
    public function __construct(private readonly BaseRepository $repository)
    {
    }

    /**
     * @return bool
     */
    public function isOpen(): bool
    {
        return $this->repository->isOpen();
    }

    /**
     * @return void
     */
    public function reopen()
    {
        $this->repository->reopen();
    }

    /**
     * @return void
     */
    public function flush()
    {
        $this->repository->flush();
    }

    /**
     * @param int $id
     * @return Transaction|null
     */
    public function find(int $id)
    {
        return $this->repository->find($id);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param object $entity
     * @param bool $flush
     * @return void
     */
    public function persist(object $entity, bool $flush = true)
    {
        $this->repository->persist($entity, $flush);
    }

    /**
     * @param object $entity
     * @param bool $flush
     * @return void
     */
    public function remove(object $entity, bool $flush = true)
    {
        $this->repository->remove($entity, $flush);
    }

}
