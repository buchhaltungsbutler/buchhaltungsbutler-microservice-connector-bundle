<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\PublicService;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Customer;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Repository\CustomerRepository;

class CustomerPersister extends BasePersister
{
    public function __construct(
        private readonly CustomerRepository $customerRepository
    )
    {
        parent::__construct($this->customerRepository);
    }

    /**
     * @param int $customerId
     * @param bool $flush
     * @return void
     * @throws CustomerNotFoundException
     */
    public function removeById(int $customerId, bool $flush = true): void
    {
        $customer = $this->customerRepository->find($customerId);
        if (empty($customer)) {
            throw new CustomerNotFoundException("couldn't find customer with id: " . $customerId);
        }
        $this->customerRepository->remove($customer, $flush);
    }

    /**
     * @param int $id
     * @return Customer|null
     */
    public function getCustomerById(int $id): ?Customer
    {
        return $this->customerRepository->find($id);
    }

    /**
     * @param string $customerId
     * @return Customer|null
     */
    public function getCustomerByCustomerId(string $customerId): ?Customer
    {
        $customers = $this->customerRepository->findBy(['customerId' => $customerId]);
        if (empty($customers)) {
            return null;
        }

        return $customers[0];
    }
}
