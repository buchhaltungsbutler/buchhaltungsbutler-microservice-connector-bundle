<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Security;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\SecurityException\AuthentificationException;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\RequestIdService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class CustomBasicAuthenticator extends AbstractAuthenticator
{

    public function __construct(
        private RequestIdService $requestIdService,
        private $microserviceId,
        private $microserviceSecret
    )
    {
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): ?bool
    {
        return true;
    }

    public function authenticate(Request $request): Passport
    {
        $credentials = $request->headers->get('Authorization');
        if (null === $credentials) {
            throw new AuthentificationException('No Authorization header provided');
        }
        $credentials = substr($credentials, 6);

        $pwRaw = $this->microserviceId . ':' . $this->microserviceSecret;
        $pw = base64_encode($pwRaw);

        if ($credentials !== $pw) {
            throw new AuthentificationException('Authorization header invalid', ['Authorization' => $credentials]);
        }

        return new SelfValidatingPassport(new UserBadge($credentials));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            'requestId' => $this->requestIdService->getRequestId(),
            'message' => $exception->getMessage(),
            // todo almi implement unique code
            'code' => $exception->getCode()
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
}