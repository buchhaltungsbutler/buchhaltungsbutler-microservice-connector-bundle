<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle;

use BuchhaltungsButler\MicroserviceConnectorBundle\DependencyInjection\AliasInterfaceImplementationsCompilerPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @codeCoverageIgnore
 */
class BuchhaltungsButlerMicroserviceConnectorBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);
        $container->addCompilerPass(new AliasInterfaceImplementationsCompilerPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, -255);
    }

}
