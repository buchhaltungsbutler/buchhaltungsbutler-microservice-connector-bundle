<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Logging;

use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\RequestIdService;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\ErrorHandler\Exception\SilencedErrorContext;

/**
 * custom Monolog instance to log to file for import to kibana
 * Reference:
 * https://github.com/markhilton/monolog-mysql/blob/master/src/Logger/Monolog/Handler/MysqlHandler.php
 *
 * @author  Alexander Mantei
 */
class KibanaLogHandler extends AbstractProcessingHandler
{
    public const DATETIME_FORMAT_KIBANA     = 'Y-m-d H:i:s.u';
    public const DEFAULT_TIMEZONE_KIBANA    = 'Europe/Berlin';
    protected string $subPath = 'permanent';

    public function __construct(
        private readonly RequestIdService $requestIdService,
        private readonly string $projectDir,
        private readonly string $microserviceId,
        private readonly string $kibanaTimezone,
        private readonly string $kibanaPath,
        $level = Logger::DEBUG,
        bool $bubble = true
    )
    {
        parent::__construct($level, $bubble);
    }

    /**     * format and write logging data to file
     *
     * @param array $record record of logging information
     *
     * @return void
     * @throws \Exception
     * @throws \Throwable
     */
    protected function write(array $record): void
    {
        // if is exception then add serialised exception info to 'extra'
        if (array_key_exists('exception', $record['context'])
            && !($record['context']['exception'] instanceof SilencedErrorContext))
        {
            $exception = $record['context']['exception'];
            if (empty($record['message'])) {
                $record['message'] = get_class($exception);
            }
            $record['extra']['exception']= [
                'message' => $exception->getMessage(),
                'exception' => get_class($exception),
                'code'    => $exception->getCode(),
                'file'    => $exception->getFile(),
                'line'    => $exception->getLine(),
                'trace'   => $exception->getTrace(),
            ];
        }

        $logEntry = [
            'id'            => uniqid('log_'),
            'logCreated'    => $this->DateTimeToKibanaString(\DateTime::createFromImmutable($record['datetime'])),
            'level'         => $record['level'],
            'requestId'     => $this->requestIdService->getRequestId(),
            'instance'      => $this->microserviceId,
            'message'       => $record['message'],
            'context'       => json_encode($record['context']),
            'extra'         => json_encode($record['extra']),
        ];

        $this->writeToFile($logEntry);
    }

    /**
     * @param array $logEntry
     * @return bool
     */
    private function writeToFile(array $logEntry): bool
    {
        $logPath = $this->kibanaPath;

        try {
            $logPath = $logPath.'/'.$this->subPath;
            $fileDirectories = explode('/', $logPath);
            $this->createDirectories(...$fileDirectories);
            @unlink($this->projectDir.'/'.$logPath.'/'.'logfile-'.date('Y-m-d-H', (new \DateTime())->sub(new \DateInterval('PT2H'))->getTimestamp()).'.log');
            $filename = 'logfile-'.date('Y-m-d-H').'.log';
            $fileResource = fopen($this->projectDir.'/'.$logPath.'/'.$filename, 'a+');
            fputs($fileResource, json_encode($logEntry)."\n");
            fclose($fileResource);
            return true;
        } catch (\Throwable $e) {
            return false;
        }
    }

    /**
     * @param string ...$fileDirectories
     */
    private function createDirectories(string ...$fileDirectories)
    {
        $logPath = $this->projectDir;
        foreach ($fileDirectories as $fileDirectory) {
            $logPath = $logPath.'/'.$fileDirectory;

            /**
             * Create file directory if not there
             */
            if (!is_dir($logPath)) {
                mkdir($logPath);
            }
        }
    }

    /**
     * converts datetime object to string from log db
     *
     * @param \DateTime $dateTime
     *
     * @return string
     * @throws \Exception
     */
    public function DateTimeToKibanaString(\DateTime $dateTime): string
    {
        return $dateTime->setTimezone(new \DateTimeZone($this->kibanaTimezone ?? self::DEFAULT_TIMEZONE_KIBANA))
            ->format(self::DATETIME_FORMAT_KIBANA);
    }
}
