<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\BaseException;

abstract class EntityNotFoundException extends BaseException
{
    const CODEBASE = [
        CustomerNotFoundException::class => 1,
        AccountNotFoundException::class => 2,
        ConnectionNotFoundException::class => 3,
    ];

    const CODE = parent::CODE + parent::CODEBASE[self::class];

    protected string $supportMessage = 'Eine unspezifizierte Datenentität wurde nicht gefunden. Das ist ein Bug und sollte niemals auftreten.';
}