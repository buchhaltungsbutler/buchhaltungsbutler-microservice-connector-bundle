<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException;

class ConnectionNotFoundException extends EntityNotFoundException
{
    const CODE = parent::CODE + parent::CODEBASE[self::class];
    protected string $supportMessage = 'In einer Anfrage an den Microservice konnte die spezifizierte Anbindung nicht gefunden werden.';
}