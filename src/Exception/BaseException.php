<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Exception;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\EntityNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestArgumentValueResolverException\RequestArgumentValueResolverException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParserException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\SecurityException\SecurityException;

abstract class BaseException extends \Exception implements BaseExceptionInterface
{
    const CODEBASE = [
        RequestArgumentValueResolverException::class => 100,
        EntityNotFoundException::class => 200,
        RequestParserException::class => 300,
        SecurityException::class => 400,
    ];

    const CODE = 0;

    protected string $loggerPrefix = 'EDPBUNDLE';
    protected string $supportMessage = 'Unspezifizierter Fehler in der Microserviceschnittstelle. Das ist ein Bug und sollte niemals auftreten.';

    /** @var array */
    protected array $data;

    /**
     * Exception constructor.
     *
     * @param string $message
     * @param array $data
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($message, $data = [], $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->data = $data;
        $this->data['supportMessage'] = $this->supportMessage;
        $this->data['loggerPrefix'] = $this->loggerPrefix;
        $this->data['code'] = static::CODE;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}