<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestArgumentValueResolverException;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\BaseException;

abstract class RequestArgumentValueResolverException extends BaseException
{
    const CODEBASE = [
        CouldNotParseRequestException::class => 1,
    ];

    const CODE = parent::CODE + parent::CODEBASE[self::class];

    protected string $supportMessage = 'Unspezifizierter Fehler im RequestArgumentValueResolver. Das ist ein Bug und sollte niemals auftreten.';
}