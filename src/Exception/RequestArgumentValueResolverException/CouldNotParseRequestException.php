<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestArgumentValueResolverException;

class CouldNotParseRequestException extends RequestArgumentValueResolverException
{
    const CODE = parent::CODE + parent::CODEBASE[self::class];
    protected string $supportMessage = 'Eine Anfrage der Anwendung an den Microservice hat einen unbekannten Typ.  Das ist ein Bug und sollte niemals auftreten.';
}