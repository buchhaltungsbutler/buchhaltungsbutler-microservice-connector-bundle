<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Exception\SecurityException;

class AuthentificationException extends SecurityException
{
    const CODE = parent::CODE + parent::CODEBASE[self::class];
    protected string $supportMessage = 'Der Authentifizierungs-Header ist ungültig.';
}