<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Exception\SecurityException;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\BaseException;

abstract class SecurityException extends BaseException
{
    const CODEBASE = [
        AuthentificationException::class => 1,
    ];

    const CODE = parent::CODE + parent::CODEBASE[self::class];

    protected string $supportMessage = 'Unspezifizierter Fehler während der Authentifizierung. Das ist ein Bug und sollte niemals auftreten.';
}