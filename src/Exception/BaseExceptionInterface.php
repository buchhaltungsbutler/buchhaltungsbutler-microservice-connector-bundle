<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Exception;

interface BaseExceptionInterface
{
    /**
     * @return array
     */
    public function getData(): array;
}