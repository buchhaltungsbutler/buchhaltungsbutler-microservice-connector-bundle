<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\BaseException;

abstract class RequestParserException extends BaseException
{
    const CODEBASE = [
        RequestParameterException::class => 1,
    ];

    const CODE = parent::CODE + parent::CODEBASE[self::class];

    protected string $supportMessage = 'Unspezifizierter Fehler in einem RequestParser. Das ist ein Bug und sollte niemals auftreten.';
}