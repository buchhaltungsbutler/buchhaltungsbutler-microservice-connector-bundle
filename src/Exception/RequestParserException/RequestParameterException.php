<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException;

class RequestParameterException extends RequestParserException
{
    const CODE = parent::CODE + parent::CODEBASE[self::class];
    protected string $supportMessage = 'Eine Anfrage der Anwendung an den Microservice hat einen ungültigen Parameter.';
}