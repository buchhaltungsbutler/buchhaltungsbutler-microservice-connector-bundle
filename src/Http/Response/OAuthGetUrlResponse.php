<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

class OAuthGetUrlResponse extends Response
{
    /**
     * @param string $url
     */
    public function __construct(private readonly string $url)
    {
        parent::__construct();
    }

    /**
     * @return array{url: string}
     */
    public function jsonSerialize(): mixed
    {
        return [
            'url' => $this->url,
        ];
    }
}
