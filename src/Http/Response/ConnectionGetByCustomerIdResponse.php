<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Connection;
use DateTimeInterface;

class ConnectionGetByCustomerIdResponse extends Response
{
    /**
     * @param Connection[] $connections
     */
    public function __construct(private readonly array $connections)
    {
        parent::__construct();
    }

    /**
     * @return array{array{
     *     connectionId: int,
     *     customerId: int,
     *     description: string,
     *     production: bool,
     *     market: string,
     *     broken: bool,
     *     metaData: array,
     *     updatedAt: string,
     *     createdAt: string
     * }}
     */
    public function jsonSerialize(): mixed
    {
        return array_map(function (Connection $connection) {
            return [
                'connectionId' => $connection->getId(),
                'customerId' => $connection->getCustomer()->getId(),
                'description' => $connection->getDescription(),
                'production' => $connection->isProduction(),
                'market' => $connection->getMarket(),
                'broken' => $connection->isBroken(),
                'metaData' => $connection->getMetaData(),
                'updatedAt' => $connection->getUpdatedAt()->format(DateTimeInterface::ATOM),
                'createdAt' => $connection->getCreatedAt()->format(DateTimeInterface::ATOM),
            ];
        }, $this->connections);
    }
}
