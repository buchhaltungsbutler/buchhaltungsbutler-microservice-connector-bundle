<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

class ConnectionCreateResponse extends Response
{
    /**
     * @param int $connectionId
     */
    public function __construct(private readonly int $connectionId)
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getConnectionId(): int
    {
        return $this->connectionId;
    }

    /**
     * @return array{connectionId:int}
     */
    public function jsonSerialize(): mixed
    {
        return [
            'connectionId' => $this->connectionId,
        ];
    }
}
