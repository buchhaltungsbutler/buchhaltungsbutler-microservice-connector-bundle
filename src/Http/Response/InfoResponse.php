<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

class InfoResponse extends Response
{
    /**
     * @param array $infoArray
     */
    public function __construct(private readonly array $infoArray)
    {
        parent::__construct();
    }

    /**
     * @return array{
     *     title: string,
     *     id: string,
     *     authMethod: array,
     *     apiVersion: float,
     *     apiFeatures: array
     * }
     */
    public function jsonSerialize(): mixed
    {
        return $this->infoArray;
    }
}
