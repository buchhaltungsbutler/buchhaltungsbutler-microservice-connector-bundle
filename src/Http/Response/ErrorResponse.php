<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

class ErrorResponse extends Response
{
    public function __construct(
        private readonly string $requestId,
        private readonly string $message,
        private readonly int $code
    )
    {
        parent::__construct(500);
        // todo variable status codes
    }

    /**
     * @return array{requestId: string, message: string, code: int}
     */
    public function jsonSerialize(): mixed
    {
        return [
            'requestId' => $this->requestId,
            'message' => $this->message,
            'code' => $this->code,
        ];
    }
}
