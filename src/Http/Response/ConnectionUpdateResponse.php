<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

class ConnectionUpdateResponse extends Response
{
    /**
     * @param int $connectionId
     */
    public function __construct(private readonly int $connectionId)
    {
        parent::__construct();
    }

    /**
     * @return array{connectionId:int}
     */
    public function jsonSerialize(): mixed
    {
        return [
            'connectionId' => $this->connectionId,
        ];
    }
}
