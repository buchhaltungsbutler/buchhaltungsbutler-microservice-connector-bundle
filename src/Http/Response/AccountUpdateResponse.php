<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

class AccountUpdateResponse extends Response
{
    /**
     * @param int $accountId
     */
    public function __construct(private readonly int $accountId)
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }


    /**
     * @return array{accountId:int}
     */
    public function jsonSerialize(): mixed
    {
        return [
            'accountId' => $this->accountId,
        ];
    }
}
