<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Account;
use DateTimeInterface;

class AccountGetByConnectionIdResponse extends Response
{
    /**
     * @param Account[] $accounts
     */
    public function __construct(private readonly array $accounts)
    {
        parent::__construct();
    }

    /**
     * @return array{array{
     *     accountId: int,
     *     connectionId: int,
     *     description: string,
     *     metaData: array,
     *     importSince: string|null,
     *     lastSync: string|null,
     *     lastSyncSuccessful: string|null,
     *     updatedAt: string,
     *     createdAt: string
     * }}
     */
    public function jsonSerialize(): mixed
    {
        return array_map(function (Account $account) {
            return [
                'accountId' => $account->getId(),
                'connectionId' => $account->getConnection()->getId(),
                'description' => $account->getDescription(),
                'metaData' => $account->getMetaData(),
                'importSince' => is_null($account->getImportSince()) ? null : $account->getImportSince()->format(DateTimeInterface::ATOM),
                'lastSync' => is_null($account->getLastSync()) ? null : $account->getLastSync()->format(DateTimeInterface::ATOM),
                'lastSyncSuccessful' => is_null($account->getLastSyncSuccessful()) ? null
                    : $account->getLastSyncSuccessful()->format(DateTimeInterface::ATOM),
                'updatedAt' => $account->getUpdatedAt()->format(DateTimeInterface::ATOM),
                'createdAt' => $account->getCreatedAt()->format(DateTimeInterface::ATOM),
            ];
        }, $this->accounts);
    }
}
