<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Connection;
use DateTimeInterface;

class ConnectionGetByIdResponse extends Response
{
    /**
     * @param Connection $connection
     */
    public function __construct(private readonly Connection $connection)
    {
        parent::__construct();
    }

    /**
     * @return array{
     *     connectionId: int,
     *     customerId: int,
     *     description: string,
     *     production: bool,
     *     market: string,
     *     broken: bool,
     *     metaData: array,
     *     updatedAt: string,
     *     createdAt: string
     * }
     */
    public function jsonSerialize(): mixed
    {
        return [
            'connectionId' => $this->connection->getId(),
            'customerId' => $this->connection->getCustomer()->getId(),
            'description' => $this->connection->getDescription(),
            'production' => $this->connection->isProduction(),
            'market' => $this->connection->getMarket(),
            'broken' => $this->connection->isBroken(),
            'metaData' => $this->connection->getMetaData(),
            'updatedAt' => $this->connection->getUpdatedAt()->format(DateTimeInterface::ATOM),
            'createdAt' => $this->connection->getCreatedAt()->format(DateTimeInterface::ATOM),
        ];
    }
}
