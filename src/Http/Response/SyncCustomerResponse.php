<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

class SyncCustomerResponse extends Response
{
    /**
     * @param string $customerId
     */
    public function __construct(private readonly string $customerId)
    {
        parent::__construct();
    }

    /**
     * @return array{customerId: string}
     */
    public function jsonSerialize(): mixed
    {
        return [
            'customerId' => $this->customerId,
        ];
    }
}
