<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Transaction;
use DateTimeInterface;

class TransactionListResponse extends Response
{
    /**
     * @param Transaction[] $transactions
     */
    public function __construct(private array $transactions)
    {
        parent::__construct();
    }

    /**
     * @return array{array{
     *     transactionId: int,
     *     accountId: int,
     *     amount: float,
     *     currency: string,
     *     fee: float,
     *     purpose: string,
     *     paymentReference: string,
     *     type: string,
     *     counterParty: string,
     *     bookedAt: string,
     *     createdAt: string,
     *     valuedAt: string,
     *     providerTransactionId: string
     * }}
     */
    public function jsonSerialize(): mixed
    {
        return array_map(function (Transaction $transaction) {
            return [
                'transactionId' => $transaction->getId(),
                'accountId' => $transaction->getAccount()->getId(),
                'amount' => $transaction->getAmount(),
                'currency' => $transaction->getCurrency(),
                'fee' => $transaction->getFeeAmount(),
                'purpose' => substr($transaction->getPurpose(), 0, 1024),
                'paymentReference' => substr($transaction->getPaymentReference(), 0, 128),
                'type' => substr($transaction->getType(), 0, 64),
                'counterParty' => substr($transaction->getCounterParty(), 0, 200),
                'bookingText' => substr($transaction->getBookingText(), 0, 500),
                'bookedAt' => $transaction->getBookedAt()->format(DateTimeInterface::ATOM),
                'createdAt' => $transaction->getCreatedAt()->format(DateTimeInterface::ATOM),
                'valuedAt' => $transaction->getValuedAt()->format(DateTimeInterface::ATOM),
                'providerTransactionId' => $transaction->getSourceTransactionId(),
            ];
        }, $this->transactions);
    }
}
