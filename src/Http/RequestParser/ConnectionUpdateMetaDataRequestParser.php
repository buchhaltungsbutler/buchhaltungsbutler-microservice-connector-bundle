<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionUpdateMetaDataRequest;
use Symfony\Component\HttpFoundation\Request;

class ConnectionUpdateMetaDataRequestParser implements RequestParser
{
    public function supports(string $requestClassName): bool
    {
        return $requestClassName === ConnectionUpdateMetaDataRequest::class;
    }

    public function parse(Request $symfonyRequest): \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
    {
        $connectionId = $symfonyRequest->attributes->get('_route_params')['connectionId'] ?? null;
        if (!is_numeric($connectionId)) {
            throw new RequestParameterException('parameter connectionId is not numeric', ['connectionId' => $connectionId]);
        }

        $requestData = $symfonyRequest->toArray();
        $metaData = $requestData['metaData'] ?? null;
        if (!is_array($metaData)) {
            throw new RequestParameterException(
                'parameter metaData is not an array',
                ['metaData' => $metaData]
            );
        }

        return new ConnectionUpdateMetaDataRequest(
            (int) $connectionId,
            $metaData,
        );
    }
}
