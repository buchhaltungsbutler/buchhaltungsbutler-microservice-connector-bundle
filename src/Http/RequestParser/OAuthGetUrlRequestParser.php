<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\OAuthGetUrlRequest;
use Symfony\Component\HttpFoundation\Request;

class OAuthGetUrlRequestParser implements RequestParser
{
    public function supports(string $requestClassName): bool
    {
        return $requestClassName === OAuthGetUrlRequest::class;
    }

    /**
     * @param Request $symfonyRequest
     * @return \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
     * @throws RequestParameterException
     */
    public function parse(Request $symfonyRequest): \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
    {
        $requestData = $symfonyRequest->toArray();

        $ruName = $requestData['ruName'] ?? null;
        if (empty($ruName)) {
            throw new RequestParameterException('parameter ruName is missing or empty', ['ruName' => $ruName]);
        }

        $customerId = $requestData['customerId'] ?? null;
        if (empty($customerId)) {
            throw new RequestParameterException('parameter customerId is missing or empty', ['customerId' => $customerId]);
        }

        $connectionId = $requestData['connectionId'] ?? null;
        if (!is_null($connectionId) && !is_numeric($connectionId)) {
            throw new RequestParameterException('parameter connectionId is not numeric', ['connectionId' => $connectionId]);
        }

        $parameters = $requestData['parameters'] ?? null;
        if (!is_array($parameters)) {
            throw new RequestParameterException(
                'parameter parameters is not an array',
                ['parameters' => $parameters]
            );
        }

        return new OAuthGetUrlRequest(
            $ruName,
            $customerId,
            is_null($connectionId) ? null : (int) $connectionId,
            $parameters,
        );
    }
}
