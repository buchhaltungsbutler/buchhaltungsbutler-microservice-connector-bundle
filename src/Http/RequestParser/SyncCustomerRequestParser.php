<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\SyncCustomerRequest;
use Symfony\Component\HttpFoundation\Request;

class SyncCustomerRequestParser implements RequestParser
{
    public function supports(string $requestClassName): bool
    {
        return $requestClassName === SyncCustomerRequest::class;
    }

    /**
     * @param Request $symfonyRequest
     * @return \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
     * @throws RequestParameterException
     */
    public function parse(Request $symfonyRequest): \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
    {
        $customerId = $symfonyRequest->attributes->get('_route_params')['customerId'] ?? null;
        if (empty($customerId)) {
            throw new RequestParameterException('parameter customerId is missing or empty', ['customerId' => $customerId]);
        }

        return new SyncCustomerRequest(
            $customerId
        );
    }
}
