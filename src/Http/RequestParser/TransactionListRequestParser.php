<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\TransactionListRequest;
use Symfony\Component\HttpFoundation\Request;

class TransactionListRequestParser implements RequestParser
{
    public function supports(string $requestClassName): bool
    {
        return $requestClassName === TransactionListRequest::class;
    }

    /**
     * @param Request $symfonyRequest
     * @return \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
     * @throws RequestParameterException
     */
    public function parse(Request $symfonyRequest): \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
    {
        $accountId = $symfonyRequest->attributes->get('_route_params')['accountId'] ?? null;
        if (!is_numeric($accountId)) {
            throw new RequestParameterException('parameter accountId is not numeric', ['accountId' => $accountId]);
        }

        $lastTransactionId = $symfonyRequest->query->get('since', 0);
        if (!is_numeric($lastTransactionId)) {
            throw new RequestParameterException('parameter since (lastTransactionId) is not numeric', ['since' => $lastTransactionId]);
        }

        $limit = $symfonyRequest->query->get('limit');
        if (!is_null($limit) && !is_numeric($limit)) {
            throw new RequestParameterException('parameter limit is not numeric', ['limit' => $limit]);
        }

        return new TransactionListRequest(
            (int) $accountId,
            (int) $lastTransactionId,
            is_null($limit) ? null : (int) $limit,
        );
    }
}
