<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionUpdateRequest;
use Symfony\Component\HttpFoundation\Request;

class ConnectionUpdateRequestParser implements RequestParser
{
    public function supports(string $requestClassName): bool
    {
        return $requestClassName === ConnectionUpdateRequest::class;
    }

    /**
     * @param Request $symfonyRequest
     * @return \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
     * @throws RequestParameterException
     */
    public function parse(Request $symfonyRequest): \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
    {
        $requestData = $symfonyRequest->toArray();

        $connectionId = $symfonyRequest->attributes->get('_route_params')['connectionId'] ?? null;
        if (!is_numeric($connectionId)) {
            throw new RequestParameterException('parameter connectionId is not numeric', ['connectionId' => $connectionId]);
        }

        $customerId = $requestData['customerId'] ?? null;
        if (empty($customerId)) {
            throw new RequestParameterException('parameter customerId is missing or empty', ['customerId' => $customerId]);
        }

        $description = $requestData['description'] ?? null;
        if (!is_string($description)) {
            throw new RequestParameterException(
                'parameter description is not a string',
                ['metaData' => $description]
            );
        }

        $parameters = $requestData['parameters'] ?? null;
        if (!is_array($parameters)) {
            throw new RequestParameterException(
                'parameter parameters is not an array',
                ['parameters' => $parameters]
            );
        }

        return new ConnectionUpdateRequest(
            (int) $connectionId,
            $customerId,
            $description,
            $parameters,
        );
    }
}
