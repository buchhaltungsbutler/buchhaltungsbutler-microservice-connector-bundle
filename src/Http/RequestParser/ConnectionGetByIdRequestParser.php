<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionGetByIdRequest;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class ConnectionGetByIdRequestParser implements RequestParser
{
    /**
     * @param string $requestClassName
     * @return bool
     */
    public function supports(string $requestClassName): bool
    {
        return $requestClassName === ConnectionGetByIdRequest::class;
    }

    /**
     * @param Request $symfonyRequest
     * @return \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
     * @throws Exception
     */
    public function parse(Request $symfonyRequest): \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
    {
        $connectionId = $symfonyRequest->attributes->get('_route_params')['connectionId'] ?? null;
        if (!is_numeric($connectionId)) {
            throw new RequestParameterException('parameter connectionId is not numeric', ['connectionId' => $connectionId]);
        }

        $limit = $symfonyRequest->query->get('limit');
        if (!is_null($limit) && !is_numeric($limit)) {
            throw new RequestParameterException('parameter limit is not numeric', ['limit' => $limit]);
        }

        return new ConnectionGetByIdRequest(
            (int) $connectionId,
            is_null($limit) ? null : (int) $limit,
        );
    }
}
