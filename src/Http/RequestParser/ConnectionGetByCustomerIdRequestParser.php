<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionGetByCustomerIdRequest;
use Symfony\Component\HttpFoundation\Request;

class ConnectionGetByCustomerIdRequestParser implements RequestParser
{
    /**
     * @param string $requestClassName
     * @return bool
     */
    public function supports(string $requestClassName): bool
    {
        return $requestClassName === ConnectionGetByCustomerIdRequest::class;
    }

    /**
     * @param Request $symfonyRequest
     * @return \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
     * @throws RequestParameterException
     */
    public function parse(Request $symfonyRequest): \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
    {
        $customerId = $symfonyRequest->attributes->get('_route_params')['customerId'] ?? null;
        if (empty($customerId)) {
            throw new RequestParameterException('parameter customerId is missing or empty', ['customerId' => $customerId]);
        }

        $limit = $symfonyRequest->query->get('limit');
        if (!is_null($limit) && !is_numeric($limit)) {
            throw new RequestParameterException('parameter limit is not numeric', ['limit' => $limit]);
        }

        return new ConnectionGetByCustomerIdRequest(
            $customerId,
            is_null($limit) ? null : (int) $limit,
        );
    }
}
