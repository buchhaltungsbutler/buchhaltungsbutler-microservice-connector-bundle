<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser;

use Symfony\Component\HttpFoundation\Request;

interface RequestParser
{
    public function supports(string $requestClassName): bool;
    public function parse(Request $symfonyRequest): \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request;
}
