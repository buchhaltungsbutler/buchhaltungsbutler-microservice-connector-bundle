<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountUpdateRequest;
use DateTimeImmutable;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class AccountUpdateRequestParser implements RequestParser
{
    /**
     * @param string $requestClassName
     * @return bool
     */
    public function supports(string $requestClassName): bool
    {
        return $requestClassName === AccountUpdateRequest::class;
    }

    /**
     * @param Request $symfonyRequest
     * @return \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
     * @throws Exception
     */
    public function parse(Request $symfonyRequest): \BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\Request
    {
        $accountId = $symfonyRequest->attributes->get('_route_params')['accountId'] ?? null;
        if (!is_numeric($accountId)) {
            throw new RequestParameterException('parameter accountId is not numeric', ['accountId' => $accountId]);
        }

        $requestData = $symfonyRequest->toArray();

        $connectionId = $requestData['connectionId'];
        if (!is_numeric($connectionId)) {
            throw new RequestParameterException('parameter connectionId is not numeric', ['connectionId' => $connectionId]);
        }

        $importSince = is_null($requestData['importSince']) ? null
            : DateTimeImmutable::createFromFormat(DATE_ATOM, $requestData['importSince']);
        if ($importSince === false) {
            throw new RequestParameterException(
                'parameter importSince is not a supported date format',
                ['importSince' => $requestData['importSince']]
            );
        }

        $description = $requestData['description'] ?? '';
        if (!is_string($description)) {
            throw new RequestParameterException(
                'parameter description is not a string',
                ['metaData' => $description]
            );
        }

        $metaData = $requestData['metaData'] ?? null;
        if (!is_null($metaData) && !is_array($metaData)) {
            throw new RequestParameterException(
                'parameter metaData is not an array',
                ['metaData' => $metaData]
            );
        }

        return new AccountUpdateRequest(
            (int) $accountId,
            (int) $connectionId,
            $importSince,
            $description,
            $metaData
        );
    }
}
