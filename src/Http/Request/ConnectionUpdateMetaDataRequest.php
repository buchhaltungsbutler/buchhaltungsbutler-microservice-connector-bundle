<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request;

class ConnectionUpdateMetaDataRequest extends Request
{
    /**
     * @param int $connectionId
     * @param array $metaData
     */
    public function __construct(
        private readonly int   $connectionId,
        private readonly array $metaData,
    )
    {
    }

    /**
     * @return int
     */
    public function getConnectionId(): int
    {
        return $this->connectionId;
    }

    /**
     * @return array
     */
    public function getMetaData(): array
    {
        return $this->metaData;
    }
}
