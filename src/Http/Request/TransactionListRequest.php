<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request;

class TransactionListRequest extends Request
{
    /**
     * @param int $accountId
     * @param int $lastTransactionId
     * @param ?int $limit
     */
    public function __construct(
        private readonly int $accountId,
        private readonly int $lastTransactionId,
        private readonly ?int $limit,
    )
    {
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getLastTransactionId(): int
    {
        return $this->lastTransactionId;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }
}
