<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request;

class ConnectionGetByIdRequest extends Request
{
    /**
     * @param int $connectionId
     * @param int|null $limit
     */
    public function __construct(
        private readonly int  $connectionId,
        private readonly ?int $limit
    )
    {
    }

    /**
     * @return int
     */
    public function getConnectionId(): int
    {
        return $this->connectionId;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }
}
