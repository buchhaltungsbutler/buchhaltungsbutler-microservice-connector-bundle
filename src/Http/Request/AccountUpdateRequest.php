<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request;

use DateTimeImmutable;

class AccountUpdateRequest extends Request
{
    /**
     * @param int $accountId
     * @param int $connectionId
     * @param DateTimeImmutable|null $importSince
     * @param string $description
     * @param array|null $metaData
     */
    public function __construct(
        private readonly int                $accountId,
        private readonly int                $connectionId,
        private readonly ?DateTimeImmutable $importSince,
        private readonly string             $description,
        private readonly ?array             $metaData
    )
    {
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }

    /**
     * @return int
     */
    public function getConnectionId(): int
    {
        return $this->connectionId;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getImportSince(): ?DateTimeImmutable
    {
        return $this->importSince;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return array|null
     */
    public function getMetaData(): ?array
    {
        return $this->metaData;
    }
}
