<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request;

class ConnectionCreateRequest extends Request
{
    /**
     * @param string $customerId
     * @param string $description
     * @param array $parameters
     */
    public function __construct(
        private readonly string $customerId,
        private readonly string $description,
        private readonly array  $parameters,
    )
    {
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }
}
