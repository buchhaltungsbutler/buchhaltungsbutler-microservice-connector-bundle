<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request;

class SyncCustomerRequest extends Request
{
    /**
     * @param string $customerId
     */
    public function __construct(private readonly string $customerId)
    {
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }
}
