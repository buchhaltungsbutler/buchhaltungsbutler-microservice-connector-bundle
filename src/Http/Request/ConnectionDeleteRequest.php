<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request;

class ConnectionDeleteRequest extends Request
{
    /**
     * @param int $connectionId
     */
    public function __construct(private readonly int $connectionId)
    {
    }

    /**
     * @return int
     */
    public function getConnectionId(): int
    {
        return $this->connectionId;
    }
}
