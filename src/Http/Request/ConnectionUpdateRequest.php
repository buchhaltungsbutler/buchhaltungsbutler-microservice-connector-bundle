<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request;

class ConnectionUpdateRequest extends Request
{
    /**
     * @param int $connectionId
     * @param string $customerId
     * @param string $description
     * @param array $parameters
     */
    public function __construct(
        private readonly int    $connectionId,
        private readonly string $customerId,
        private readonly string $description,
        private readonly array $parameters,
    )
    {
    }

    /**
     * @return int
     */
    public function getConnectionId(): int
    {
        return $this->connectionId;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }
}
