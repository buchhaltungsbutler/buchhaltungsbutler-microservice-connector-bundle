<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request;

class ConnectionGetByCustomerIdRequest extends Request
{
    /**
     * @param string $customerId
     * @param int|null $limit
     */
    public function __construct(
        private readonly string $customerId,
        private readonly ?int   $limit
    )
    {
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }
}
