<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request;

class OAuthGetUrlRequest extends Request
{
    /**
     * @param string $ruName
     * @param string $customerId
     * @param int|null $connectionId
     * @param array $parameters
     */
    public function __construct(
        private readonly string $ruName,
        private readonly string $customerId,
        private readonly ?int   $connectionId,
        private readonly array  $parameters,
    )
    {
    }

    /**
     * @return string
     */
    public function getRuName(): string
    {
        return $this->ruName;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return int
     */
    public function getConnectionId(): ?int
    {
        return $this->connectionId;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }
}
