<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\DependencyInjection;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\RequestParser;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\BaseInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class BuchhaltungsButlerMicroserviceConnectorExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../../config')
        );

        $loader->load('services.yaml');

        $container->registerForAutoconfiguration(BaseInterface::class)
            ->addTag('bhb.external_data_provider.interface');

        $container->registerForAutoconfiguration(RequestParser::class)
            ->addTag('bhb.external_data_provider.request_parser');
    }
}
