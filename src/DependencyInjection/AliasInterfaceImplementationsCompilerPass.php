<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\DependencyInjection;

use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\BaseInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Aliases interface implementations with the interface name
 *
 * That means, if App\Service\OAuth extends the BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\OAuthInterface,
 * it is also accesible unter the interfaces name in the container.
 *
 */
class AliasInterfaceImplementationsCompilerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        $taggedServiceIds = $container->findTaggedServiceIds('bhb.external_data_provider.interface');
        foreach ($taggedServiceIds as $serviceId => $tags) {
            $definition = $container->getDefinition($serviceId);
            $parents = class_parents($definition->getClass());
            foreach ($parents as $parent) {
                if ($parent === BaseInterface::class) {
                    continue;
                }

                if (str_starts_with($parent, 'BuchhaltungsButler\\MicroserviceConnectorBundle\\PublicInterfaces\\')) {
                    $container->setAlias($parent, $serviceId);
                }
            }
        }
    }
}
