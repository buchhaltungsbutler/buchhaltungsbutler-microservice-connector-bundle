<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\AccountNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Service\TransactionService;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use Doctrine\ORM\Tools\ToolsException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Service\TransactionService
 */
final class TransactionServiceTest extends KernelTestCase
{
    use FixtureAwareTrait;

    /**
     * @return void
     * @throws ToolsException
     */
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->cleanDB('microservice_connector_bundle');
        $this->transactionService = $this->getContainer()->get(TransactionService::class);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testTransactionService(): void
    {
        // SETUP ====================================
        $customerId1 = 'customer1';
        $customerId2 = 'customer2';
        $givenCustomers = $this->givenCustomer(['customerId' => $customerId1], ['customerId' => $customerId2]);
        $givenConnections = $this->givenConnection(['customer' => $givenCustomers[0]], ['customer' => $givenCustomers[1]]);
        $givenAccounts = $this->givenAccount(
            ['connection' => $givenConnections[0]],
            ['connection' => $givenConnections[1]],
        );
        $givenTransactions = $this->givenTransaction(
            ['account' => $givenAccounts[0], 'sourceTransactionId' => '0'],
            ['account' => $givenAccounts[0], 'sourceTransactionId' => '1', 'deleted' => true],
            ['account' => $givenAccounts[0], 'sourceTransactionId' => '2'],
            ['account' => $givenAccounts[1], 'sourceTransactionId' => '3'],
            ['account' => $givenAccounts[1], 'sourceTransactionId' => '4', 'deleted' => true],
            ['account' => $givenAccounts[1], 'sourceTransactionId' => '5']
        );

        // EXECUTION AND EVALUATION ====================================
        $transactions = $this->transactionService->getTransactionsByAccountId($givenAccounts[0]->getId());
        $this->assertCount(2, $transactions);
        $this->assertEquals($givenTransactions[0], $transactions[0]);
        $this->assertEquals($givenTransactions[2], $transactions[1]);

        $transactions = $this->transactionService->getTransactionsByAccountId($givenAccounts[0]->getId(), 1);
        $this->assertCount(1, $transactions);
        $this->assertEquals($givenTransactions[2], $transactions[0]);

        $transactions = $this->transactionService->getTransactionsByAccountId($givenAccounts[0]->getId(), 0, 1);
        $this->assertCount(1, $transactions);
        $this->assertEquals($givenTransactions[0], $transactions[0]);


        $transactions = $this->transactionService->getTransactionsByAccountId($givenAccounts[1]->getId());
        $this->assertCount(2, $transactions);
        $this->assertEquals($givenTransactions[3], $transactions[0]);
        $this->assertEquals($givenTransactions[5], $transactions[1]);

        $transactions = $this->transactionService->getTransactionsByAccountId($givenAccounts[1]->getId(), 4);
        $this->assertCount(1, $transactions);
        $this->assertEquals($givenTransactions[5], $transactions[0]);

        $transactions = $this->transactionService->getTransactionsByAccountId($givenAccounts[1]->getId(), 2, 3);
        $this->assertCount(2, $transactions);
        $this->assertEquals($givenTransactions[3], $transactions[0]);
        $this->assertEquals($givenTransactions[5], $transactions[1]);

        $this->expectException(AccountNotFoundException::class);
        $this->transactionService->getTransactionsByAccountId(3);
    }
}
