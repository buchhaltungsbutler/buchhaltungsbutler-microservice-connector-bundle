<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests;

use Symfony\Component\HttpFoundation\Response;

/**
 * a trait that introduces extended assertion functions
 * can only be used with symfony KernelTestCase class or derivatives
 */
trait ExtendedAssertionTrait
{
    private function assertResponseSuccessful(): Response
    {
        $this->assertResponseIsSuccessful();
        return $this->client->getResponse();
    }

    private function assertResponseJson(Response $response): array
    {
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        $content = $response->getContent();
        $this->assertJson($content);
        return json_decode($content, true);
    }
}

