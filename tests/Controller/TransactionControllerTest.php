<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\ExtendedAssertionTrait;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Transaction;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\DatabaseTimeFormatter;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Controller\TransactionController
 */

class TransactionControllerTest extends WebTestCase
{
    use FixtureAwareTrait;
    use ExtendedAssertionTrait;
    private KernelBrowser $client;

    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        $this->client = static::createClient();
        $this->cleanDB('microservice_connector_bundle');
    }

    public function compareTransactionWithResponse(array $transactionData, array $response)
    {
        $this->assertSameSize($transactionData, $response);

        $count = 0;
        foreach ($transactionData as $transaction) {
            /** @var Transaction $transaction */
            $this->assertEquals($transaction['account'], $response[$count]['accountId']);
            $this->assertEquals($transaction['amount'], $response[$count]['amount']);
            $this->assertEquals($transaction['currency'], $response[$count]['currency']);
            $this->assertEquals($transaction['sourceTransactionId'], $response[$count]['providerTransactionId']);
            $this->assertEquals($transaction['purpose'], $response[$count]['purpose']);
            $this->assertEquals($transaction['paymentReference'], $response[$count]['paymentReference']);
            $this->assertEquals($transaction['type'], $response[$count]['type']);
            $this->assertEquals($transaction['counterParty'], $response[$count]['counterParty']);
            $this->assertEquals($transaction['bookingText'], $response[$count]['bookingText']);
            $this->assertEquals(
                DatabaseTimeFormatter::dateTimeToString($transaction['bookedAt']),
                DatabaseTimeFormatter::dateTimeToString(new \DateTime($response[$count]['bookedAt'])));
            $this->assertEquals(
                DatabaseTimeFormatter::dateTimeToString($transaction['valuedAt']),
                DatabaseTimeFormatter::dateTimeToString(new \DateTime($response[$count]['valuedAt'])));
            $this->assertEquals(
                DatabaseTimeFormatter::dateTimeToString($transaction['createdAt']),
                DatabaseTimeFormatter::dateTimeToString(new \DateTime($response[$count]['createdAt'])));
            $count++;
        }
    }

    /**
     * @dataProvider provideTestTransactionGetByAccountId
     *
     * @param array $given
     * @param array $expected
     * @return void
     */
    public function testTransactionGetByAccountId(array $given, array $expected): void
    {
        // SETUP ====================================

        $customers = $this->givenCustomer(['customerId' => $given['customerId'],]);
        $connections = $this->givenConnection(['customer' => $customers[0]]);

        // replace connection Ids by objects
        $accountData = $given['accountData'];
        $counter = 0;
        foreach ($accountData as $accountDatum) {
            $accountData[$counter++]['connection'] = $connections[$accountDatum['connection']];
        }
        $accounts = $this->givenAccount(...$accountData);

        // replace account Ids by objects
        $transactionData = $given['transactionData'];
        $counter = 0;
        foreach ($transactionData as $transactionDatum) {
            $transactionData[$counter++]['account'] = $accounts[$transactionDatum['account']];
        }

        $this->givenTransaction(...$transactionData);

        // EXECUTION ====================================
        $this->client->request('GET', '/accounts/'.$given['accountId'].'/transactions?since='.$given['since'].'&limit='.$given['limit']);

        // RESULT ANALYSIS ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->compareTransactionWithResponse($expected, $responseData);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function provideTestTransactionGetByAccountId(): array
    {
        return [
            'only return transactions in correct account' => [
                'given' => [
                    'customerId' => 'testCustomerId',
                    'accountId' => 1,
                    'since' => 0,
                    'limit' => 100,
                    'accountData' => [
                        [
                            'connection' => 0,
                        ],
                        [
                            'connection' => 0,
                        ],
                    ],
                    'transactionData' => [
                        [
                            'account' => 0,
                            'sourceTransactionId' => 'testSourceTransactionId1',
                        ],
                        [
                            'account' => 0,
                            'amount' => 1.2,
                            'currency' => 'CHF',
                            'amountOriginal' => 2.0,
                            'currencyOriginal' => 'EUR',
                            'exchangeRate' => 1.2,
                            'sourceTransactionId' => 'testSourceTransactionId2',
                            'purpose' => 'testPurpose2',
                            'paymentReference' => 'testPaymentReference2',
                            'type' => 'testType2',
                            'counterParty' => 'testCounterParty2',
                            'bookingText' => 'testBookingText2',
                            'bookedAt' => new \DateTimeImmutable($this->testTime),
                            'valuedAt' => new \DateTimeImmutable($this->testTime),
                            'createdAt' => new \DateTimeImmutable($this->testTime),
                        ],
                        [
                            'account' => 1,
                            'sourceTransactionId' => 'testSourceTransactionId3',
                        ],
                    ],
                ],
                'expected' => [
                    [
                        'id' => 1,
                        'account' => 1,
                        'amount' => 1.1,
                        'currency' => 'EUR',
                        'feeAmount' => -1.0,
                        'sourceTransactionId' => 'testSourceTransactionId1',
                        'purpose' => 'testPurpose',
                        'paymentReference' => 'testPaymentReference',
                        'type' => 'testType',
                        'counterParty' => 'testCounterParty',
                        'bookingText' => 'testBookingText',
                        'bookedAt' => new \DateTimeImmutable($this->testTime),
                        'valuedAt' => new \DateTimeImmutable($this->testTime),
                        'createdAt' => new \DateTimeImmutable($this->testTime),
                        'deleted' => false,
                    ],
                    [
                        'id' => 2,
                        'account' => 1,
                        'amount' => 1.2,
                        'currency' => 'CHF',
                        'amountOriginal' => 2.0,
                        'currencyOriginal' => 'EUR',
                        'exchangeRate' => 1.2,
                        'sourceTransactionId' => 'testSourceTransactionId2',
                        'purpose' => 'testPurpose2',
                        'paymentReference' => 'testPaymentReference2',
                        'type' => 'testType2',
                        'counterParty' => 'testCounterParty2',
                        'bookingText' => 'testBookingText2',
                        'bookedAt' => new \DateTimeImmutable($this->testTime),
                        'valuedAt' => new \DateTimeImmutable($this->testTime),
                        'createdAt' => new \DateTimeImmutable($this->testTime),
                        'deleted' => false,
                    ],
                ],
            ],
            'only return #limit transactions after since Id' => [
                'given' => [
                    'customerId' => 'testCustomerId',
                    'accountId' => 1,
                    'since' => 1,
                    'limit' => 2,
                    'accountData' => [
                        [
                            'connection' => 0,
                        ],
                    ],
                    'transactionData' => [
                        [
                            'account' => 0,
                            'sourceTransactionId' => 'testSourceTransactionId1',
                        ],
                        [
                            'account' => 0,
                            'sourceTransactionId' => 'testSourceTransactionId2',
                        ],
                        [
                            'account' => 0,
                            'sourceTransactionId' => 'testSourceTransactionId3',
                        ],
                        [
                            'account' => 0,
                            'sourceTransactionId' => 'testSourceTransactionId4',
                        ],
                    ],
                ],
                'expected' => [
                    [
                        'id' => 2,
                        'account' => 1,
                        'amount' => 1.1,
                        'currency' => 'EUR',
                        'feeAmount' => -1.0,
                        'sourceTransactionId' => 'testSourceTransactionId2',
                        'purpose' => 'testPurpose',
                        'paymentReference' => 'testPaymentReference',
                        'type' => 'testType',
                        'counterParty' => 'testCounterParty',
                        'bookingText' => 'testBookingText',
                        'bookedAt' => new \DateTimeImmutable($this->testTime),
                        'valuedAt' => new \DateTimeImmutable($this->testTime),
                        'createdAt' => new \DateTimeImmutable($this->testTime),
                        'deleted' => false,
                    ],
                    [
                        'id' => 3,
                        'account' => 1,
                        'amount' => 1.1,
                        'currency' => 'EUR',
                        'feeAmount' => -1.0,
                        'sourceTransactionId' => 'testSourceTransactionId3',
                        'purpose' => 'testPurpose',
                        'paymentReference' => 'testPaymentReference',
                        'type' => 'testType',
                        'counterParty' => 'testCounterParty',
                        'bookingText' => 'testBookingText',
                        'bookedAt' => new \DateTimeImmutable($this->testTime),
                        'valuedAt' => new \DateTimeImmutable($this->testTime),
                        'createdAt' => new \DateTimeImmutable($this->testTime),
                        'deleted' => false,
                    ],
                ],
            ],
        ];
    }
}
