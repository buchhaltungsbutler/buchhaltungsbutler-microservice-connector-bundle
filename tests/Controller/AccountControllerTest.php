<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\ExtendedAssertionTrait;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Account;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\AccountPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\DatabaseTimeFormatter;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Controller\AccountController
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\AccountInterface
 */
class AccountControllerTest extends WebTestCase
{
    use FixtureAwareTrait;
    use ExtendedAssertionTrait;
    private KernelBrowser $client;


    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        $this->client = static::createClient();
        $this->cleanDB('microservice_connector_bundle');
    }

    public function compareAccountWithResponse(array $accountData, array $response, bool $testForCreatedUpdated = true)
    {
        $this->assertSameSize($accountData, $response);

        $count = 0;
        foreach ($accountData as $account) {
            /** @var Account $account  */
            $this->assertEquals($account->getConnection()->getId(), $response[$count]['connectionId']);
            $this->assertEquals($account->getDescription(), $response[$count]['description']);
            $this->assertEquals(
                DatabaseTimeFormatter::dateTimeToString($account->getImportSince()),
                DatabaseTimeFormatter::dateTimeToString(new \DateTime($response[$count]['importSince'])));
            $this->assertEquals(
                DatabaseTimeFormatter::dateTimeToString($account->getLastSync()),
                DatabaseTimeFormatter::dateTimeToString(new \DateTime($response[$count]['lastSync'])));
            $this->assertEquals(
                DatabaseTimeFormatter::dateTimeToString($account->getLastSyncSuccessful()),
                DatabaseTimeFormatter::dateTimeToString(new \DateTime($response[$count]['lastSyncSuccessful'])));

            if ($testForCreatedUpdated) {
                $this->assertEquals(
                    DatabaseTimeFormatter::dateTimeToString($account->getUpdatedAt()),
                    DatabaseTimeFormatter::dateTimeToString(new \DateTime($response[$count]['updatedAt'])));
                $this->assertEquals(
                    DatabaseTimeFormatter::dateTimeToString($account->getCreatedAt()),
                    DatabaseTimeFormatter::dateTimeToString(new \DateTime($response[$count]['createdAt'])));
            }

            $count++;
        }
    }

    public function testAccountGetByCustomerIdSuccessful(): void
    {
        // SETUP ====================================
        $customerId1 = 'customer1';
        $customerId2 = 'customer2';
        $customers = $this->givenCustomer(['customerId' => $customerId1], ['customerId' => $customerId2]);
        $connections = $this->givenConnection(['customer' => $customers[0]], ['customer' => $customers[1]]);
        $this->givenAccount(
            ['connection' => $connections[0]],
            ['connection' => $connections[0]],
            ['connection' => $connections[0]]
        );
        $accountData = $this->givenAccount(
            ['connection' => $connections[1]],
            ['connection' => $connections[1]],
            ['connection' => $connections[1]]
        );

        // EXECUTION ====================================
        $this->client->request('GET', '/customers/'. $customerId2 .'/accounts');

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->compareAccountWithResponse($accountData, $responseData);
    }

    public function testAccountGetByConnectionIdSuccessful(): void
    {
        // SETUP ====================================
        $customerId1 = 'customer1';
        $customers = $this->givenCustomer(['customerId' => $customerId1]);
        $connections = $this->givenConnection(['customer' => $customers[0]], ['customer' => $customers[0]]);
        $this->givenAccount(
            ['connection' => $connections[0]],
            ['connection' => $connections[0]],
            ['connection' => $connections[0]]
        );
        $accountData = $this->givenAccount(
            ['connection' => $connections[1]],
            ['connection' => $connections[1]],
            ['connection' => $connections[1]]
        );

        // EXECUTION ====================================
        $this->client->request('GET', '/connections/'. $connections[1]->getId() .'/accounts');

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->compareAccountWithResponse($accountData, $responseData);
    }

    public function testAccountUpdateSuccessful(): void
    {
        // SETUP ====================================
        $customers = $this->givenCustomer([]);
        $connections = $this->givenConnection(['customer' => $customers[0]]);
        $this->givenAccount(['connection' => $connections[0]]);

        $requestParameters = [
            'connectionId' => 1,
            'description' => 'testDescription5001',
            'importSince' => (new \DateTimeImmutable('2022-01-12 00:00:00'))->format(DATE_ATOM),
        ];

        // EXECUTION ====================================
        $this->client->request('PUT', '/accounts/1', [], [], ['HTTP_Content-Type' => 'application/json'], json_encode($requestParameters));

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->assertEquals(1, $responseData['accountId']);

        /** @var AccountPersister $accountPersister */
        $accountPersister = $this->getContainer()->get(AccountPersister::class);

        $account = $accountPersister->getAccountById(1);

        $this->assertEquals($requestParameters['connectionId'], $account->getConnection()->getId());
        $this->assertNotEquals($requestParameters['description'], $account->getDescription());
        $this->assertEquals(
            DatabaseTimeFormatter::dateTimeToString(new \DateTimeImmutable($requestParameters['importSince'])),
            DatabaseTimeFormatter::dateTimeToString($account->getImportSince()));

        $this->assertEqualsWithDelta(
            DatabaseTimeFormatter::dateTimeToString(new \DateTimeImmutable($this->testTime)),
            DatabaseTimeFormatter::dateTimeToString($account->getUpdatedAt()),
            5.0
        );
    }
}
