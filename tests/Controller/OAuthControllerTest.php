<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\ExtendedAssertionTrait;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\RequestIdService;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Controller\OAuthController
 */

class OAuthControllerTest extends WebTestCase
{
    use FixtureAwareTrait;
    use ExtendedAssertionTrait;
    private KernelBrowser $client;


    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        $this->client = static::createClient();
    }

    public function testOAuthGetUrlSuccessful(): void
    {
        // SETUP ====================================

        $parameters = [
          'ruName' => 'testRUNAME',
          'customerId' => 'testCustomerId',
          'connectionId' => null,
          'parameters' => ['name' => 'name']
        ];

        $requestIdService = $this->getContainer()->get(RequestIdService::class);

        $url = 'https://oauth.test.com'
            .'&redirect_uri=' . urlencode($parameters['ruName'])
            .'&state=' . urlencode($parameters['customerId']
                . '|' . $parameters['connectionId']
                . '|' . $requestIdService->getRequestId()
                . '|' . $parameters['parameters']['name'] ?? '');

        // EXECUTION ====================================
        $this->client->request('POST', '/oauth', [], [], ['HTTP_Content-Type' => 'application/json'], json_encode($parameters));

        // RESULT ANALYSIS ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->assertEquals($url, $responseData['url']);
    }
}
