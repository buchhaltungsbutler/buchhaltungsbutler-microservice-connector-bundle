<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\ExtendedAssertionTrait;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Controller\SyncController
 */

class SyncControllerTest extends WebTestCase
{
    use FixtureAwareTrait;
    use ExtendedAssertionTrait;
    private KernelBrowser $client;

    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        $this->client = static::createClient();
    }

    public function testSyncCustomerSuccessful(): void
    {
        // SETUP ====================================
        $customerId = 'customer1';
        $this->givenCustomer(['customerId' => $customerId]);

        // EXECUTION ====================================
        $this->client->request('POST', '/customers/'. $customerId .'/sync');

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->assertEquals($customerId, $responseData['customerId']);
    }
}
