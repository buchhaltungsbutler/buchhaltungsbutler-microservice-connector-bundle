<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\ExtendedAssertionTrait;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Connection;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\ConnectionPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\DatabaseTimeFormatter;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\EntityConverterService;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\Tools\ToolsException;
use Exception;
use ReflectionException;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Controller\ConnectionController
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\ConnectionInterface
 */
class ConnectionControllerTest extends WebTestCase
{
    use FixtureAwareTrait;
    use ExtendedAssertionTrait;
    private KernelBrowser $client;


    /**
     * @return void
     * @throws ToolsException
     */
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        $this->client = static::createClient();
        $this->cleanDB('microservice_connector_bundle');
    }

    /**
     * @param array $connectionData
     * @param array $response
     * @param bool $testForCreatedUpdated
     * @return void
     * @throws Exception
     */
    public function compareConnectionWithResponse(array $connectionData, array $response, bool $testForCreatedUpdated = true): void
    {
        $this->assertSameSize($connectionData, $response);

        $count = 0;
        foreach ($connectionData as $connection) {
            /** @var Connection $connection */
            $this->assertEquals($connection->getCustomer()->getId(), $response[$count]['customerId']);
            $this->assertEquals($connection->getDescription(), $response[$count]['description']);
            $this->assertEquals($connection->isProduction(), $response[$count]['production']);
            $this->assertEquals($connection->getMarket(), $response[$count]['market']);
            $this->assertEquals($connection->isBroken(), $response[$count]['broken']);
            $this->assertEquals($connection->getMetaData(), $response[$count]['metaData']);

            if ($testForCreatedUpdated) {
                $this->assertEquals(
                    DatabaseTimeFormatter::dateTimeToString($connection->getUpdatedAt()),
                    DatabaseTimeFormatter::dateTimeToString(new DateTime($response[$count]['updatedAt'])));
                $this->assertEquals(
                    DatabaseTimeFormatter::dateTimeToString($connection->getCreatedAt()),
                    DatabaseTimeFormatter::dateTimeToString(new DateTime($response[$count]['createdAt'])));
            }

            $count++;
        }
    }

    public function testConnectionCreateSuccessful(): void
    {
        // SETUP ====================================
        $connectionParameters = [
            'production' => true,
            'market' => 'DE',
            'broken' => false,
            'oauthResponse' => [
                'spapi_oauth_code' => 'RHNulpEBmyDTClSaRhAS',
            ],
        ];

        $requestParameters = [
            'customerId' => 'testCustomerId',
            'description' => 'testDescription',
            'parameters' => $connectionParameters,
        ];

        // EXECUTION ====================================
        $this->client->request('POST', '/connections', [], [], ['HTTP_Content-Type' => 'application/json'], json_encode($requestParameters));

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->assertEquals(1, $responseData['connectionId'] ?? 0);
    }

    public function testConnectionDeleteSuccessful(): void
    {
        // SETUP ====================================
        $customers = $this->givenCustomer([]);
        $connectionData = $this->givenConnection(['customer' => $customers[0]]);
        $connectionId = $connectionData[0]->getId();

        // EXECUTION ====================================
        $this->client->request('DELETE', '/connections/' . $connectionId);

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->assertEquals($connectionId, $responseData['connectionId'] ?? 0);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testConnectionGetByCustomerIdSuccessful(): void
    {
        // SETUP ====================================
        $customerId1 = 'customer1';
        $customerId2 = 'customer2';
        $customers = $this->givenCustomer(['customerId' => $customerId1], ['customerId' => $customerId2]);
        $this->givenConnection(
            ['customer' => $customers[0], 'description' => 'connection1'],
            ['customer' => $customers[0], 'description' => 'connection2'],
            ['customer' => $customers[0], 'description' => 'connection3']
        );
        $connectionData = $this->givenConnection(
            ['customer' => $customers[1], 'description' => 'connection4'],
            ['customer' => $customers[1], 'description' => 'connection5'],
            ['customer' => $customers[1], 'description' => 'connection6']
        );

        // EXECUTION ====================================
        $this->client->request('GET', '/customers/'. $customerId2 .'/connections');

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->compareConnectionWithResponse($connectionData, $responseData);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testConnectionGetByIdSuccessful(): void
    {
        // SETUP ====================================
        $customerId1 = 'customer1';
        $customers = $this->givenCustomer(['customerId' => $customerId1]);
        $connectionData = $this->givenConnection(
            ['customer' => $customers[0], 'description' => 'connection1'],
            ['customer' => $customers[0], 'description' => 'connection2'],
            ['customer' => $customers[0], 'description' => 'connection3']
        );
        $connectionIndex = 1;
        $connectionId = $connectionData[$connectionIndex]->getId();

        // EXECUTION ====================================
        $this->client->request('GET', '/connections/'. $connectionId);

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->compareConnectionWithResponse([$connectionData[$connectionIndex]], [$responseData]);
    }

    /**
     * @return void
     * @throws CustomerNotFoundException
     * @throws ReflectionException
     * @throws Exception
     */
    public function testConnectionUpdateSuccessful(): void
    {
        // SETUP ====================================
        $customerId = 'testCustomerId';
        $customers = $this->givenCustomer(['customerId' => $customerId,]);
        $givenConnections = $this->givenConnection(
            ['customer' => $customers[0], 'description' => 'connection1'],
            ['customer' => $customers[0], 'description' => 'connection2'],
            ['customer' => $customers[0], 'description' => 'connection3']
        );
        $connectionIndex = 1;
        $connectionId = $givenConnections[$connectionIndex]->getId();

        $now = new DateTimeImmutable();

        $connectionParameters = [
            'production' => true,
            'market' => 'AU',
            'broken' => false,
            'metaData' => [],
            'accessToken' => 'testAccessToken2',
            'accessTokenExpireTime' => $now->format(DATE_ATOM),
            'refreshToken' => 'testRefreshToken2',
            'refreshTokenExpireTime' => $now->format(DATE_ATOM),
        ];

        $requestParameters = [
            'customerId' => $customerId,
            'description' => 'testDescription',
            'parameters' => $connectionParameters,
        ];

        $additionalParameters = [
            'customer' => $customers[0],
            'id' => 0,
            'accessExpireTime' => $now,
            'refreshExpireTime' => $now,
            'updatedAt' => $now,
            'createdAt' => $now,
        ];

        $connectionData = array_merge($requestParameters, $connectionParameters, $additionalParameters);
        unset($connectionData['parameters']);

        $givenConnections[$connectionIndex] = EntityConverterService::fromArray(Connection::class, $connectionData);

        // EXECUTION ====================================
        $this->client->request(
            'PUT',
            '/connections/'. $connectionId,
            [],
            [],
            ['HTTP_Content-Type' => 'application/json'],
            json_encode($requestParameters)
        );

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->assertEquals($connectionId, $responseData['connectionId']);

        /** @var ConnectionPersister $connectionPersister */
        $connectionPersister = $this->getContainer()->get(ConnectionPersister::class);

        $connections = $connectionPersister->getConnectionsByCustomerId($customerId);

        $resultingConnections = array_map(function (Connection $connection) {
            $connectionArray = EntityConverterService::toArray($connection);
            $connectionArray['customerId'] = $connectionArray['customer']->getId();
            return $connectionArray;
        }, $connections);


        $this->compareConnectionWithResponse($givenConnections, $resultingConnections, false);
    }

    /**
     * @return void
     * @throws CustomerNotFoundException
     * @throws ReflectionException
     * @throws Exception
     */
    public function testConnectionUpdateMetaDataSuccessful(): void
    {
        // SETUP ====================================
        $customerId = 'testCustomerId';
        $customers = $this->givenCustomer(['customerId' => $customerId,]);
        $givenConnections = $this->givenConnection(
            ['customer' => $customers[0], 'description' => 'connection1'],
            ['customer' => $customers[0], 'description' => 'connection2'],
            ['customer' => $customers[0], 'description' => 'connection3']
        );
        $connectionIndex = 1;
        $connectionId = $givenConnections[$connectionIndex]->getId();

        $metaData = [
            'metaData' => [
                'meta1' => 2,
                'meta2' => '1',
                'meta3' => ['meta3'],
            ],
        ];
        $givenConnections[$connectionIndex]->setMetadata($metaData['metaData']);

        // EXECUTION ====================================
        $this->client->request(
            'PUT',
            '/connections/'.$connectionId.'/metadata',
            [],
            [],
            ['HTTP_Content-Type' => 'application/json'],
            json_encode($metaData)
        );

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->assertEquals($connectionId, $responseData['connectionId']);

        /** @var ConnectionPersister $connectionPersister */
        $connectionPersister = $this->getContainer()->get(ConnectionPersister::class);

        $connections = $connectionPersister->getConnectionsByCustomerId($customerId);

        $resultingConnections = array_map(function (Connection $connection) {
            $connectionArray = EntityConverterService::toArray($connection);
            $connectionArray['customerId'] = $connectionArray['customer']->getId();
            return $connectionArray;
        }, $connections);


        $this->compareConnectionWithResponse($givenConnections, $resultingConnections, false);
    }

    public function testConnectionUpdateMetaDataUnsuccessful(): void
    {
        // SETUP ====================================
        $connectionId = 1;

        // EXECUTION ====================================
        $this->client->request(
            'PUT',
            '/connections/'.$connectionId.'/metadata',
            [],
            [],
            ['HTTP_Content-Type' => 'application/json'],
            json_encode(['metaData' => []])
        );

        // EVALUATION ====================================
        $this->assertResponseStatusCodeSame(500);
        $responseData = $this->assertResponseJson($this->client->getResponse());
        $this->assertEquals('connection not found for id: '.$connectionId, $responseData['message']);
    }
}
