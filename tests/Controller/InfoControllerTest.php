<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\ExtendedAssertionTrait;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Controller\InfoController
 */

class InfoControllerTest extends WebTestCase
{
    use FixtureAwareTrait;
    use ExtendedAssertionTrait;
    private KernelBrowser $client;

    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        $this->client = static::createClient();
    }

    public function testInfoGetSuccessful(): void
    {
        // SETUP ====================================
        $info = [
            'title' => 'BHB Test Microservice',
            'id' => 'test',
            'authMethod' => ['oauth2'],
            'apiVersion' => 1.0,
            'apiFeatures' => ['transactions']
        ];

        // EXECUTION ====================================
        $this->client->request('GET', '/info');

        // EVALUATION ====================================
        $response = $this->assertResponseSuccessful();
        $responseData = $this->assertResponseJson($response);
        $this->assertEquals($info, $responseData);
    }
}
