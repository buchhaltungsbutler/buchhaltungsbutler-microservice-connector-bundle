<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\AccountNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\ConnectionNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\TransactionPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use Doctrine\ORM\Tools\ToolsException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\TransactionPersister
 */
final class TransactionPersisterTest extends KernelTestCase
{
    use FixtureAwareTrait;

    /**
     * @return void
     * @throws ToolsException
     */
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->cleanDB('microservice_connector_bundle');
        $this->transactionPersister = $this->getContainer()->get(TransactionPersister::class);
    }

    /**
     * @return void
     * @throws AccountNotFoundException
     * @throws ConnectionNotFoundException
     * @throws CustomerNotFoundException
     */
    public function testTransactionPersister(): void
    {
        // SETUP ====================================
        $customerId1 = 'customer1';
        $customerId2 = 'customer2';
        $givenCustomers = $this->givenCustomer(['customerId' => $customerId1], ['customerId' => $customerId2]);
        $givenConnections = $this->givenConnection(['customer' => $givenCustomers[0]], ['customer' => $givenCustomers[1]]);
        $givenAccounts = $this->givenAccount(
            ['connection' => $givenConnections[0]],
            ['connection' => $givenConnections[0]],
            ['connection' => $givenConnections[0]],
            ['connection' => $givenConnections[1]],
            ['connection' => $givenConnections[1]],
            ['connection' => $givenConnections[1]]
        );
        $givenTransactions = $this->givenTransaction(
            ['account' => $givenAccounts[0], 'sourceTransactionId' => '0'],
            ['account' => $givenAccounts[1], 'sourceTransactionId' => '1', 'deleted' => true],
            ['account' => $givenAccounts[2], 'sourceTransactionId' => '2'],
            ['account' => $givenAccounts[3], 'sourceTransactionId' => '3'],
            ['account' => $givenAccounts[4], 'sourceTransactionId' => '4', 'deleted' => true],
            ['account' => $givenAccounts[5], 'sourceTransactionId' => '5']
        );

        // EXECUTION AND EVALUATION ====================================

        // findBySourceIdAndAccount
        $transactionId = 0;
        foreach ($givenAccounts as $account) {
            $transaction = $this->transactionPersister->findBySourceIdAndAccount((string) $transactionId, $account->getId());
            if ($givenTransactions[$transactionId]->isDeleted()) {
                $this->assertNull($transaction);
            } else {
                $this->assertEquals($givenTransactions[$transactionId], $transaction);
            }
            $transactionId++;
        }

        $this->expectException(AccountNotFoundException::class);
        $this->transactionPersister->findBySourceIdAndAccount((string) $transactionId, 7);
    }
}
