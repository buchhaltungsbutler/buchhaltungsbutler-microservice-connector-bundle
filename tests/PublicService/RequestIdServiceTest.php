<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\RequestIdService;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use ReflectionException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class EntityConverterTest extends KernelTestCase
{
    use FixtureAwareTrait;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
    }

    /**
     * @return void
     * @throws ReflectionException
     */
    public function testEntityConverter(): void
    {
        // SETUP ====================================
        $requestIdService = $this->getContainer()->get(RequestIdService::class);

        // EXECUTION AND EVALUATION ====================================
        $requestIdService->getRequestId();
        $requestId = 'testRequestId';
        $requestIdService->setRequestId($requestId);
        $this->assertEquals($requestId, $requestIdService->getRequestId());

    }
}
