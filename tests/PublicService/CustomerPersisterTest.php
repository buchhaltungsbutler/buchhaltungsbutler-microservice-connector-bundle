<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\CustomerPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use Doctrine\ORM\Tools\ToolsException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\CustomerPersister
 */
final class CustomerPersisterTest extends KernelTestCase
{
    use FixtureAwareTrait;

    /**
     * @return void
     * @throws ToolsException
     */
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->cleanDB('microservice_connector_bundle');
        $this->customerPersister = $this->getContainer()->get(CustomerPersister::class);
    }

    /**
     * @return void
     * @throws CustomerNotFoundException
     */
    public function testCustomerPersister(): void
    {
        // SETUP ====================================
        $customerId1 = 'customer1';
        $customerId2 = 'customer2';
        $givenCustomers = $this->givenCustomer(['customerId' => $customerId1], ['customerId' => $customerId2]);

        // EXECUTION AND EVALUATION ====================================

        // getCustomerById
        $customer = $this->customerPersister->getCustomerById(1);
        $this->assertEquals($givenCustomers[0], $customer);
        $customer = $this->customerPersister->getCustomerById(2);
        $this->assertEquals($givenCustomers[1], $customer);

        // getCustomerByCustomerId
        $customer = $this->customerPersister->getCustomerByCustomerId($customerId1);
        $this->assertEquals($givenCustomers[0], $customer);
        $customer = $this->customerPersister->getCustomerByCustomerId($customerId2);
        $this->assertEquals($givenCustomers[1], $customer);
        $customer = $this->customerPersister->getCustomerByCustomerId('no customer');
        $this->assertNull($customer);

        // removeById
        $this->customerPersister->removeById(1);
        $customer = $this->customerPersister->getCustomerById(1);
        $this->assertNull($customer);

        $this->expectException(CustomerNotFoundException::class);
        $this->customerPersister->removeById(1);
    }

}
