<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Account;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Connection;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Customer;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Transaction;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\EntityConverterService;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use Doctrine\ORM\Tools\ToolsException;
use ReflectionException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class EntityConverterServiceTest extends KernelTestCase
{
    use FixtureAwareTrait;

    /**
     * @return void
     * @throws ToolsException
     */
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->cleanDB('microservice_connector_bundle');
    }

    /**
     * @return void
     * @throws ReflectionException
     */
    public function testEntityConverter(): void
    {
        // SETUP ====================================
        $customerId1 = 'customer1';
        $customerId2 = 'customer2';
        $givenCustomers = $this->givenCustomer(['customerId' => $customerId1], ['customerId' => $customerId2]);
        $givenConnections = $this->givenConnection(['customer' => $givenCustomers[0]], ['customer' => $givenCustomers[1]]);
        $givenAccounts = $this->givenAccount(
            ['connection' => $givenConnections[0]],
            ['connection' => $givenConnections[0]],
            ['connection' => $givenConnections[0]],
            ['connection' => $givenConnections[1]],
            ['connection' => $givenConnections[1]],
            ['connection' => $givenConnections[1]]
        );
        $givenTransactions = $this->givenTransaction(
            ['account' => $givenAccounts[0]],
            ['account' => $givenAccounts[1]],
            ['account' => $givenAccounts[2]],
            ['account' => $givenAccounts[3]],
            ['account' => $givenAccounts[4]],
            ['account' => $givenAccounts[5]]
        );

        // EXECUTION AND EVALUATION ====================================
        foreach ($givenCustomers as $customer) {
            $this->assertEquals($customer, EntityConverterService::fromArray(Customer::class, EntityConverterService::toArray($customer)));
        }
        foreach ($givenConnections as $connection) {
            $this->assertEquals($connection, EntityConverterService::fromArray(Connection::class, EntityConverterService::toArray($connection)));
        }
        foreach ($givenAccounts as $account) {
            $this->assertEquals($account, EntityConverterService::fromArray(Account::class, EntityConverterService::toArray($account)));
        }
        foreach ($givenTransactions as $transaction) {
            $this->assertEquals($transaction, EntityConverterService::fromArray(Transaction::class, EntityConverterService::toArray($transaction)));
        }
    }
}
