<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\DatabaseTimeFormatter;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use DateTimeImmutable;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\DatabaseTimeFormatter
 */
final class DatabaseTimeFormatterTest extends KernelTestCase
{
    use FixtureAwareTrait;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testDatabaseTimeFormatter(): void
    {
        // SETUP ====================================
        $dateTimeString = '2022-01-01 00:00:00';
        $dateTimeImmutable = new DateTimeImmutable($dateTimeString . ' +0000');
        $dateTimeAtom = $dateTimeImmutable->format(DATE_ATOM);

        // EXECUTION AND EVALUATION ====================================
        $this->assertEquals($dateTimeString, DatabaseTimeFormatter::dateTimeToString($dateTimeImmutable));
        $this->assertEquals($dateTimeImmutable, DatabaseTimeFormatter::stringToDateTimeImmutable($dateTimeAtom));
    }

}
