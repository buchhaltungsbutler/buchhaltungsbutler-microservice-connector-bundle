<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\ConnectionNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\ConnectionPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use Doctrine\ORM\Tools\ToolsException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\ConnectionPersister
 */
final class ConnectionPersisterTest extends KernelTestCase
{
    use FixtureAwareTrait;

    /**
     * @return void
     * @throws ToolsException
     */
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->cleanDB('microservice_connector_bundle');
        $this->connectionPersister = $this->getContainer()->get(ConnectionPersister::class);
    }

    /**
     * @return void
     * @throws ConnectionNotFoundException
     * @throws CustomerNotFoundException
     */
    public function testConnectionPersister(): void
    {
        // SETUP ====================================
        $customerId1 = 'customer1';
        $customerId2 = 'customer2';
        $customers = $this->givenCustomer(['customerId' => $customerId1], ['customerId' => $customerId2]);
        $givenConnections = $this->givenConnection(['customer' => $customers[0]], ['customer' => $customers[1]]);
        $connectionId = 2;

        // EXECUTION AND EVALUATION ====================================

        // getConnectionsByCustomerId
        $connections = $this->connectionPersister->getConnectionsByCustomerId($customerId1);
        $this->assertEquals($givenConnections[0], $connections[0]);
        $connections = $this->connectionPersister->getConnectionsByCustomerId($customerId2);
        $this->assertEquals($givenConnections[1], $connections[0]);
        try {
            $this->expectException(CustomerNotFoundException::class);
            $this->connectionPersister->getConnectionsByCustomerId('no customer');
        } catch (CustomerNotFoundException $e) {}

        // getConnectionById
        $connection = $this->connectionPersister->getConnectionById($connectionId);
        $this->assertEquals($givenConnections[1], $connection);

        // updateMetaData
        $testKey = 'testKey';
        $testData = ['testData'];
        $metaData = [$testKey => $testData];
        $this->connectionPersister->updateMetaData($connection, $testKey, $testData);
        $connection = $this->connectionPersister->getConnectionById($connectionId);
        $this->assertEquals($metaData, $connection->getMetaData());

        // removeById
        $this->connectionPersister->removeById($connectionId);
        $connection = $this->connectionPersister->getConnectionById($connectionId);
        $this->assertNull($connection);

        $this->expectException(ConnectionNotFoundException::class);
        $this->connectionPersister->removeById($connectionId);
    }

}
