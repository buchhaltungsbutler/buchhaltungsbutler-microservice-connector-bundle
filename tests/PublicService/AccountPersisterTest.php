<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\AccountNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\ConnectionNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\AccountPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\Tests\FixtureAwareTrait;
use Doctrine\ORM\Tools\ToolsException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\AccountPersister
 */
final class AccountPersisterTest extends KernelTestCase
{
    use FixtureAwareTrait;

    /**
     * @return void
     * @throws ToolsException
     */
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->cleanDB('microservice_connector_bundle');
        $this->accountPersister = $this->getContainer()->get(AccountPersister::class);
    }

    /**
     * @return void
     * @throws AccountNotFoundException
     * @throws ConnectionNotFoundException
     * @throws CustomerNotFoundException
     */
    public function testAccountPersister(): void
    {
        // SETUP ====================================
        $customerId1 = 'customer1';
        $customerId2 = 'customer2';
        $customers = $this->givenCustomer(['customerId' => $customerId1], ['customerId' => $customerId2]);
        $connections = $this->givenConnection(['customer' => $customers[0]], ['customer' => $customers[1]]);
        $givenAccountsCustomer1 = $this->givenAccount(
            ['connection' => $connections[0]],
            ['connection' => $connections[0]],
            ['connection' => $connections[0]]
        );
        $givenAccountsCustomer2 = $this->givenAccount(
            ['connection' => $connections[1]],
            ['connection' => $connections[1]],
            ['connection' => $connections[1]]
        );
        $accountId = 2;

        // EXECUTION AND EVALUATION ====================================

        // getAccountsByCustomerId
        $accounts = $this->accountPersister->getAccountsByCustomerId($customerId2);
        $this->assertEquals($givenAccountsCustomer2, $accounts);
        try {
            $this->expectException(CustomerNotFoundException::class);
            $this->accountPersister->getAccountsByCustomerId('no customer');
        } catch (CustomerNotFoundException $e) {}

        // getAccountsByConnectionId
        $accounts = $this->accountPersister->getAccountsByConnectionId($connections[1]->getId());
        $this->assertEquals($givenAccountsCustomer2, $accounts);
        try {
            $this->expectException(ConnectionNotFoundException::class);
            $this->accountPersister->getAccountsByConnectionId(666);
        } catch (ConnectionNotFoundException $e) {}

        // getAccountById
        $account = $this->accountPersister->getAccountById($accountId);
        $this->assertEquals($givenAccountsCustomer1[1], $account);

        // updateMetaData
        $testKey = 'testKey';
        $testData = ['testData'];
        $metaData = [$testKey => $testData];
        $this->accountPersister->updateMetaData($account, $testKey, $testData);
        $account = $this->accountPersister->getAccountById($accountId);
        $this->assertEquals($metaData, $account->getMetaData());

        // removeById
        $this->accountPersister->removeById($accountId);
        $account = $this->accountPersister->getAccountById($accountId);
        $this->assertNull($account);

        $this->expectException(AccountNotFoundException::class);
        $this->accountPersister->removeById($accountId);
    }

}
