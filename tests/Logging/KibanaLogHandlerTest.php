<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Logging\KibanaLogHandler;
use DateTimeImmutable;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Throwable;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Logging\KibanaLogHandler
 */
final class KibanaLogHandlerTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
    }

    private function removeDirRecursively(string $dir): void
    {
        try {
            $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
        } catch (Throwable $e) {
            return;
        }
        $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dir);
    }

    public function testLogSuccessful(): void
    {
        // SETUP ====================================
        $this->removeDirRecursively('kibana');

        $logRecord = [
          'context' => [
              'exception' => new CustomerNotFoundException('')
          ],
          'level' => 100,
          'message' => '',
          'extra' => ['anotherMessage' => 'anotherTestMessage'],
          'datetime' => new DateTimeImmutable('2022-01-01 00:00:00')
        ];

        $logHandler = $this->getContainer()->get(KibanaLogHandler::class);

        // EXECUTION ====================================
        $logHandler->handle($logRecord);

        // EVALUATION ====================================
        $jsonData = file_get_contents('kibana/permanent/'. 'logfile-'.date('Y-m-d-H').'.log');
        $data = json_decode($jsonData, true);
        $data['extra'] = json_decode($data['extra'], true);
        $this->assertEquals('2022-01-01 00:00:00.000000', $data['logCreated']);
        $this->assertEquals(100, $data['level']);
        $this->assertEquals('testId', $data['instance']);
        $this->assertEquals(
            'BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException',
            $data['message']
        );
        $this->assertEquals('{"exception":{}}', $data['context']);
        $this->assertEquals('anotherTestMessage', $data['extra']['anotherMessage']);
        $this->assertEquals('', $data['extra']['exception']['message']);
        $this->assertEquals(0, $data['extra']['exception']['code']);
    }

    public function testLogUnsuccessful(): void
    {
        // SETUP ====================================
        $this->removeDirRecursively('kibana');
        $fileResource = fopen('kibana', 'a+');
        fclose($fileResource);

        $logRecord = [
            'context' => [
                'exception' => new CustomerNotFoundException('')
            ],
            'level' => 100,
            'message' => '',
            'extra' => ['anotherMessage' => 'anotherTestMessage'],
            'datetime' => new DateTimeImmutable('2022-01-01 00:00:00')
        ];

        $logHandler = $this->getContainer()->get(KibanaLogHandler::class);

        // EXECUTION ====================================
        $logHandler->handle($logRecord);

        // EVALUATION ====================================
        unlink('kibana');
        $this->assertFalse(file_exists('kibana'));
    }
}
