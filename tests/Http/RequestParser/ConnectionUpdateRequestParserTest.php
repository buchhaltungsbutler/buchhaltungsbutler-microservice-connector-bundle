<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionUpdateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionUpdateRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionUpdateRequestParser
 */
final class ConnectionUpdateRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->connectionUpdateRequestParser = new ConnectionUpdateRequestParser();
    }

    public function testSupportsConnectionUpdateRequestSuccessful(): void
    {
        $this->assertTrue($this->connectionUpdateRequestParser->supports(ConnectionUpdateRequest::class));
    }

    private function dataProviderParseConnectionUpdateRequestSuccessful(): array
    {
        return [
            'successful' => [1, 'customerId', 'description', []],
            'empty description' => [1, 'customerId', '', []],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionUpdateRequestSuccessful
     */
    public function testParseConnectionUpdateRequestSuccessful(
        int    $connectionId,
        string $customerId,
        string $description,
        array $parameters
    ): void
    {
        // SETUP ====================================
        $request = new Request(
            [],
            [],
            ['_route_params' => ['connectionId' => $connectionId]],
            [],
            [],
            [],
            json_encode([
                'customerId' => $customerId,
                'description' => $description,
                'parameters' => $parameters
            ])
        );

        // EXECUTION ====================================
        $connectionUpdateRequest = $this->connectionUpdateRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($connectionId, $connectionUpdateRequest->getConnectionId());
        $this->assertEquals($customerId, $connectionUpdateRequest->getCustomerId());
        $this->assertEquals($description, $connectionUpdateRequest->getDescription());
        $this->assertEquals($parameters, $connectionUpdateRequest->getParameters());
    }

    private function dataProviderParseConnectionUpdateRequestUnsuccessful(): array
    {
        return [
            'null connectionId' => [null, 'customerId', 'description', []],
            'empty connectionId' => ['', 'customerId', 'description', []],
            'non numeric connectionId' => ['error', 'customerId', 'description', []],
            'wrong type connectionId' => [false, 'customerId', 'description', []],
            'null customerId' => ['1', null, 'description', []],
            'empty customerId' => ['1', '', 'description', []],
            'wrong type customerId' => ['1', false, 'description', []],
            'wrong type description' => ['1', 'customerId', false, []],
            'wrong type parameters' => ['1', 'customerId', 'description', false],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionUpdateRequestUnsuccessful
     */
    public function testParseConnectionUpdateRequestUnsuccessful(
        $connectionId,
        $customerId,
        $description,
        $parameters
    ): void
    {
        // SETUP ====================================
        $routeParams = [];
        $requestBody = [];
        if (!is_null($connectionId)) {
            $routeParams['_route_params']['connectionId'] = $connectionId;
        }
        if (!is_null($customerId)) {
            $requestBody['customerId'] = $customerId;
        }
        if (!is_null($description)) {
            $requestBody['description'] = $description;
        }
        if (!is_null($parameters)) {
            $requestBody['parameters'] = $parameters;
        }
        $request = new Request(
            [],
            [],
            $routeParams,
            [],
            [],
            [],
            json_encode($requestBody)
        );

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->connectionUpdateRequestParser->parse($request);

        // EVALUATION ====================================
    }
}