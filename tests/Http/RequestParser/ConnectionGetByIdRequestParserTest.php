<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionGetByIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionGetByIdRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionGetByIdRequestParser
 */
final class ConnectionGetByIdRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->connectionGetByIdRequestParser = new ConnectionGetByIdRequestParser();
    }

    public function testSupportsConnectionGetByIdRequestSuccessful(): void
    {
        $this->assertTrue($this->connectionGetByIdRequestParser->supports(ConnectionGetByIdRequest::class));
    }

    private function dataProviderParseConnectionGetByIdRequestSuccessful(): array
    {
        return [
            'null limit' => ['1', null],
            'limit' => ['1', '1'],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionGetByIdRequestSuccessful
     */
    public function testParseConnectionGetByIdRequestSuccessful(string $connectionId, ?string $limit): void
    {
        // SETUP ====================================
        $request = new Request(['limit' => $limit], [], ['_route_params' => ['connectionId' => $connectionId]]);

        // EXECUTION ====================================
        $connectionGetByIdRequest = $this->connectionGetByIdRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($connectionId, $connectionGetByIdRequest->getConnectionId());
        $this->assertEquals(empty($limit) ? null : (int) $limit, $connectionGetByIdRequest->getLimit());
    }

    private function dataProviderParseConnectionGetByIdRequestUnsuccessful(): array
    {
        return [
            'empty connectionId' => ['', null],
            'non numeric connectionId' => ['error', null],
            'wrong type connectionId' => [false, null],
            'empty limit' => ['1', ''],
            'non numeric limit' => ['1', 'error'],
            'wrong type limit' => ['1', false],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionGetByIdRequestUnsuccessful
     */
    public function testParseConnectionGetByIdRequestUnsuccessful($connectionId, $limit): void
    {
        // SETUP ====================================
        $request = new Request(['limit' => $limit], [], ['_route_params' => ['connectionId' => $connectionId]]);

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->connectionGetByIdRequestParser->parse($request);

        // EVALUATION ====================================
    }
}
