<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\OAuthGetUrlRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\OAuthGetUrlRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\OAuthGetUrlRequestParser
 */
final class OAuthGetUrlRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->OAuthGetUrlRequestParser = new OAuthGetUrlRequestParser();
    }

    public function testSupportsOAuthGetUrlRequestSuccessful(): void
    {
        $this->assertTrue($this->OAuthGetUrlRequestParser->supports(OAuthGetUrlRequest::class));
    }

    private function dataProviderParseOAuthGetUrlRequestSuccessful(): array
    {
        return [
            'successful' => ['ruName', 'customerId', 1, []],
            'null connectionId' => ['ruName', 'customerId', null, []],
        ];
    }

    /**
     * @dataProvider dataProviderParseOAuthGetUrlRequestSuccessful
     */
    public function testParseOAuthGetUrlRequestSuccessful(
        string $ruName,
        string $customerId,
        ?int   $connectionId,
        array  $parameters
    ): void
    {
        // SETUP ====================================
        $request = new Request(
            [],
            [],
            [],
            [],
            [],
            [],
            json_encode([
                'ruName' => $ruName,
                'customerId' => $customerId,
                'connectionId' => $connectionId,
                'parameters' => $parameters
            ])
        );

        // EXECUTION ====================================
        $connectionUpdateRequest = $this->OAuthGetUrlRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($connectionId, $connectionUpdateRequest->getConnectionId());
        $this->assertEquals($customerId, $connectionUpdateRequest->getCustomerId());
        $this->assertEquals($ruName, $connectionUpdateRequest->getRuName());
        $this->assertEquals($parameters, $connectionUpdateRequest->getParameters());
    }

    private function dataProviderParseOAuthGetUrlRequestUnsuccessful(): array
    {
        return [
            'null ruName' => [null, 'customerId', null, []],
            'empty ruName' => ['', 'customerId', null, []],
            'wrong type ruName' => [false, 'customerId', null, []],
            'null customerId' => ['ruName', null, null, []],
            'empty customerId' => ['ruName', '', null, []],
            'wrong type customerId' => ['ruName', false, null, []],
            'non numeric connectionId' => ['ruName', 'customerId', 'error', []],
            'wrong type connectionId' => ['ruName', 'customerId', false, []],
            'null parameters' => ['ruName', 'customerId', null, null],
            'wrong type parameters' => ['ruName', 'customerId', null, false],
        ];
    }

    /**
     * @dataProvider dataProviderParseOAuthGetUrlRequestUnsuccessful
     */
    public function testParseOAuthGetUrlRequestUnsuccessful(
        $ruName,
        $customerId,
        $connectionId,
        $parameters
    ): void
    {
        // SETUP ====================================
        $requestBody = [];
        if (!is_null($ruName)) {
            $requestBody['ruName'] = $ruName;
        }
        if (!is_null($customerId)) {
            $requestBody['customerId'] = $customerId;
        }
        if (!is_null($connectionId)) {
            $requestBody['connectionId'] = $connectionId;
        }
        if (!is_null($parameters)) {
            $requestBody['parameters'] = $parameters;
        }
        $request = new Request(
            [],
            [],
            [],
            [],
            [],
            [],
            json_encode($requestBody)
        );

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->OAuthGetUrlRequestParser->parse($request);

        // EVALUATION ====================================
    }
}