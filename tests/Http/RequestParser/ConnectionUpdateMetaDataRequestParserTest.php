<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionUpdateMetaDataRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionUpdateMetaDataRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionUpdateMetaDataRequestParser
 */
final class ConnectionUpdateMetaDataRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->connectionUpdateMetaDataRequestParser = new ConnectionUpdateMetaDataRequestParser();
    }

    public function testSupportsConnectionUpdateMetaDataRequestSuccessful(): void
    {
        $this->assertTrue($this->connectionUpdateMetaDataRequestParser->supports(ConnectionUpdateMetaDataRequest::class));
    }

    private function dataProviderParseConnectionUpdateMetaDataRequestSuccessful(): array
    {
        return [
            'successful' => [1, []],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionUpdateMetaDataRequestSuccessful
     */
    public function testParseConnectionUpdateMetaDataRequestSuccessful(
        int $connectionId,
        ?array $metaData
    ): void
    {
        // SETUP ====================================
        $request = new Request(
            [],
            [],
            ['_route_params' => ['connectionId' => $connectionId]],
            [],
            [],
            [],
            json_encode([
                'metaData' => $metaData
            ])
        );

        // EXECUTION ====================================
        $connectionMetaDataUpdateRequest = $this->connectionUpdateMetaDataRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($connectionId, $connectionMetaDataUpdateRequest->getConnectionId());
        $this->assertEquals($metaData, $connectionMetaDataUpdateRequest->getMetaData());
    }

    private function dataProviderParseConnectionUpdateMetaDataRequestUnsuccessful(): array
    {
        return [
            'null connectionId' => [null, []],
            'non numeric connectionId' => ['error', []],
            'wrong type connectionId' => [false, []],
            'null metaData' => ['1', null],
            'wrong type metaData' => ['1', false],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionUpdateMetaDataRequestUnsuccessful
     */
    public function testParseConnectionUpdateMetaDataRequestUnsuccessful(
        $connectionId,
        $metaData
    ): void
    {
        // SETUP ====================================
        $routeParams = [];
        $requestBody = [];
        if (!is_null($connectionId)) {
            $routeParams['_route_params']['connectionId'] = $connectionId;
        }
        if (!is_null($metaData)) {
            $requestBody['metaData'] = $metaData;
        }
        $request = new Request(
            [],
            [],
            $routeParams,
            [],
            [],
            [],
            json_encode($requestBody)
        );

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->connectionUpdateMetaDataRequestParser->parse($request);

        // EVALUATION ====================================
    }
}