<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\TransactionListRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\TransactionListRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\TransactionListRequestParser
 */
final class TransactionListRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->transactionListRequestParser = new TransactionListRequestParser();
    }

    public function testSupportsTransactionListRequestSuccessful(): void
    {
        $this->assertTrue($this->transactionListRequestParser->supports(TransactionListRequest::class));
    }

    private function dataProviderParseTransactionListRequestSuccessful(): array
    {
        return [
            'null limit' => [1, 1, null],
            'limit' => [1, 1, 1],
        ];
    }

    /**
     * @dataProvider dataProviderParseTransactionListRequestSuccessful
     */
    public function testParseTransactionListRequestSuccessful(
        int $accountId,
        int $lastTransactionId,
        ?int $limit
    ): void
    {
        // SETUP ====================================
        $request = new Request(
            [
                'since' => $lastTransactionId,
                'limit' => $limit,
            ],
            [],
            ['_route_params' => ['accountId' => $accountId]]
        );

        // EXECUTION ====================================
        $transactionListRequest = $this->transactionListRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($accountId, $transactionListRequest->getAccountId());
        $this->assertEquals($lastTransactionId, $transactionListRequest->getLastTransactionId());
        $this->assertEquals(empty($limit) ? null : (int) $limit, $transactionListRequest->getLimit());
    }

    private function dataProviderParseTransactionListRequestUnsuccessful(): array
    {
        return [
            'null accountId' => [null, 1, null],
            'empty accountId' => ['', 1, null],
            'non numeric accountId' => ['error', 1, null],
            'wrong type accountId' => [false, 1, null],
            'null lastTransactionId' => [1, null, null],
            'empty lastTransactionId' => [1, '', null],
            'non numeric lastTransactionId' => [1, 'error', null],
            'wrong type lastTransactionId' => [1, false, null],
            'empty limit' => [1, 1, ''],
            'non numeric limit' => [1, 1, 'error'],
            'wrong type limit' => [1, 1, false],
        ];
    }

    /**
     * @dataProvider dataProviderParseTransactionListRequestUnsuccessful
     */
    public function testParseTransactionListRequestUnsuccessful($accountId, $lastTransactionId, $limit): void
    {
        // SETUP ====================================
        $request = new Request(
            [
                'since' => $lastTransactionId,
                'limit' => $limit
            ],
            [],
            ['_route_params' => ['accountId' => $accountId]]
        );

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->transactionListRequestParser->parse($request);

        // EVALUATION ====================================
    }
}
