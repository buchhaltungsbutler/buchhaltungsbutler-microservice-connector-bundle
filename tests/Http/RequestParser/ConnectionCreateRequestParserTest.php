<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionCreateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionCreateRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionCreateRequestParser
 */
final class ConnectionCreateRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->connectionCreateRequestParser = new ConnectionCreateRequestParser();
    }

    public function testSupportsConnectionCreateRequestSuccessful(): void
    {
        $this->assertTrue($this->connectionCreateRequestParser->supports(ConnectionCreateRequest::class));
    }

    private function dataProviderParseConnectionCreateRequestSuccessful(): array
    {
        return [
            'successful' => ['customerId', 'description', []],
            'empty description' => ['customerId', '', []],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionCreateRequestSuccessful
     */
    public function testParseConnectionCreateRequestSuccessful(
        string $customerId,
        string $description,
        array $parameters
    ): void
    {
        // SETUP ====================================
        $request = new Request(
            [],
            [],
            [],
            [],
            [],
            [],
            json_encode([
                'customerId' => $customerId,
                'description' => $description,
                'parameters' => $parameters
            ])
        );

        // EXECUTION ====================================
        $connectionCreateRequest = $this->connectionCreateRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($customerId, $connectionCreateRequest->getCustomerId());
        $this->assertEquals($description, $connectionCreateRequest->getDescription());
        $this->assertEquals($parameters, $connectionCreateRequest->getParameters());
    }

    private function dataProviderParseConnectionCreateRequestUnsuccessful(): array
    {
        return [
            'null customerId' => [null, 'description', []],
            'empty customerId' => ['', 'description', []],
            'wrong type customerId' => [false , 'description', []],
            'null description' => ['customerId', null, []],
            'wrong type description' => ['customerId', false, []],
            'null parameters' => ['customerId', 'description', null],
            'wrong type parameters' => ['customerId', 'description', false],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionCreateRequestUnsuccessful
     */
    public function testParseConnectionCreateRequestUnsuccessful(
        $customerId,
        $description,
        $parameters
    ): void
    {
        // SETUP ====================================
        $requestBody = [];
        if (!is_null($customerId)) {
            $requestBody['customerId'] = $customerId;
        }
        if (!is_null($description)) {
            $requestBody['description'] = $description;
        }
        if (!is_null($parameters)) {
            $requestBody['parameters'] = $parameters;
        }
        $request = new Request(
            [],
            [],
            [],
            [],
            [],
            [],
            json_encode($requestBody)
        );

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->connectionCreateRequestParser->parse($request);

        // EVALUATION ====================================
    }
}