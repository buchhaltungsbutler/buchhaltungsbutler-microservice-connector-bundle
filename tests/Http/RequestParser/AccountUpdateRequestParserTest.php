<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountUpdateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\AccountUpdateRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\AccountUpdateRequestParser
 */
final class AccountUpdateRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->accountUpdateRequestParser = new AccountUpdateRequestParser();
    }

    public function testSupportsAccountUpdateRequestSuccessful(): void
    {
        $this->assertTrue($this->accountUpdateRequestParser->supports(AccountUpdateRequest::class));
    }

    private function dataProviderParseAccountUpdateRequestSuccessful(): array
    {
        return [
            'null metaData' => [1, 1, '2020-01-01T00:00:00+0000', 'description', null],
            'metaData' => [1, 1, '2020-01-01T00:00:00+0000', 'description', []],
        ];
    }

    /**
     * @dataProvider dataProviderParseAccountUpdateRequestSuccessful
     */
    public function testParseAccountUpdateRequestSuccessful(
        int $accountId,
        int $connectionId,
        string $importSince,
        string $description,
        ?array $metaData
    ): void
    {
        // SETUP ====================================
        $request = new Request(
            [],
            [],
            ['_route_params' => ['accountId' => $accountId]],
            [],
            [],
            [],
            json_encode([
                'connectionId' => $connectionId,
                'importSince' => $importSince,
                'description' => $description,
                'metaData' => $metaData
            ])
        );

        // EXECUTION ====================================
        $accountUpdateRequest = $this->accountUpdateRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($accountId, $accountUpdateRequest->getAccountId());
        $this->assertEquals($connectionId, $accountUpdateRequest->getConnectionId());
        $this->assertEquals(new \DateTimeImmutable($importSince), $accountUpdateRequest->getImportSince());
        $this->assertEquals($description, $accountUpdateRequest->getDescription());
        $this->assertEquals($metaData, $accountUpdateRequest->getMetaData());
    }

    private function dataProviderParseAccountUpdateRequestUnsuccessful(): array
    {
        return [
            'empty accountId' => [null, '1', '2020-01-01T00:00:00+00:00', 'description', null],
            'non numeric accountId' => ['error', '1', '2020-01-01T00:00:00+00:00', 'description', null],
            'non numeric connectionId' => ['1', 'error', '2020-01-01T00:00:00+00:00', 'description', null],
            'non supported importSince format' => ['1', '1', '2020-01-01', 'description', null],
            'non string description' => ['1', '1', '2020-01-01T00:00:00+00:00', true, null],
            'non array metaData' => ['1', '1', '2020-01-01T00:00:00+00:00', 'description', true],
        ];
    }

    /**
     * @dataProvider dataProviderParseAccountUpdateRequestUnsuccessful
     */
    public function testParseAccountUpdateRequestUnsuccessful(
        $accountId,
        $connectionId,
        $importSince,
        $description,
        $metaData
    ): void
    {
        // SETUP ====================================
        $routeParams = [];
        $requestBody = [];
        if (!is_null($accountId)) {
            $routeParams['_route_params']['accountId'] = $accountId;
        }
        if (!is_null($connectionId)) {
            $requestBody['connectionId'] = $connectionId;
        }
        if (!is_null($importSince)) {
            $requestBody['importSince'] = $importSince;
        }
        if (!is_null($description)) {
            $requestBody['description'] = $description;
        }
        if (!is_null($metaData)) {
            $requestBody['metaData'] = $metaData;
        }
        $request = new Request(
            [],
            [],
            $routeParams,
            [],
            [],
            [],
            json_encode($requestBody)
        );

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->accountUpdateRequestParser->parse($request);

        // EVALUATION ====================================
    }
}