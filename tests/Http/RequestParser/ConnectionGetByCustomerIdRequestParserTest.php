<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionGetByCustomerIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionGetByCustomerIdRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionGetByCustomerIdRequestParser
 */
final class ConnectionGetByCustomerIdRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->connectionGetByCustomerIdRequestParser = new ConnectionGetByCustomerIdRequestParser();
    }

    public function testSupportsAccountGetByConnectionIdRequestSuccessful(): void
    {
        $this->assertTrue($this->connectionGetByCustomerIdRequestParser->supports(ConnectionGetByCustomerIdRequest::class));
    }

    private function dataProviderParseConnectionGetByCustomerIdRequestSuccessful(): array
    {
        return [
            'null limit' => ['customerId', null],
            'limit' => ['customerId', '1'],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionGetByCustomerIdRequestSuccessful
     */
    public function testParseConnectionGetByCustomerIdRequestSuccessful(string $customerId, ?string $limit): void
    {
        // SETUP ====================================
        $request = new Request(['limit' => $limit], [], ['_route_params' => ['customerId' => $customerId]]);

        // EXECUTION ====================================
        $connectionGetByConnectionIdRequest = $this->connectionGetByCustomerIdRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($customerId, $connectionGetByConnectionIdRequest->getCustomerId());
        $this->assertEquals(empty($limit) ? null : (int) $limit, $connectionGetByConnectionIdRequest->getLimit());
    }

    private function dataProviderParseConnectionGetByCustomerIdRequestUnsuccessful(): array
    {
        return [
            'empty customerId' => ['', null],
            'empty limit' => ['customerId', ''],
            'non numeric limit' => ['customerId', 'error'],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionGetByCustomerIdRequestUnsuccessful
     */
    public function testParseConnectionGetByCustomerIdRequestUnsuccessful($connectionId, $limit): void
    {
        // SETUP ====================================
        $request = new Request(['limit' => $limit], [], ['_route_params' => ['customerId' => $connectionId]]);

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->connectionGetByCustomerIdRequestParser->parse($request);

        // EVALUATION ====================================
    }
}
