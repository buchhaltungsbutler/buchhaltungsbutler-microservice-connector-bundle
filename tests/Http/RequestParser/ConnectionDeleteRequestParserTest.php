<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionDeleteRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionDeleteRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\ConnectionDeleteRequestParser
 */
final class ConnectionDeleteRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->connectionCreateRequestParser = new ConnectionDeleteRequestParser();
    }

    public function testSupportsConnectionDeleteRequestSuccessful(): void
    {
        $this->assertTrue($this->connectionCreateRequestParser->supports(ConnectionDeleteRequest::class));
    }

    private function dataProviderParseConnectionDeleteRequestSuccessful(): array
    {
        return [
            'successful' => [1],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionDeleteRequestSuccessful
     */
    public function testParseConnectionDeleteRequestSuccessful(
        int $connectionId
    ): void
    {
        // SETUP ====================================
        $request = new Request(
            [],
            [],
            ['_route_params' => ['connectionId' => $connectionId]],
        );

        // EXECUTION ====================================
        $connectionCreateRequest = $this->connectionCreateRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($connectionId, $connectionCreateRequest->getConnectionId());
    }

    private function dataProviderParseConnectionDeleteRequestUnsuccessful(): array
    {
        return [
            'null connectionId' => [null],
            'empty connectionId' => [''],
            'non numeric connectionId' => ['error'],
        ];
    }

    /**
     * @dataProvider dataProviderParseConnectionDeleteRequestUnsuccessful
     */
    public function testParseConnectionDeleteRequestUnsuccessful(
        $connectionId
    ): void
    {
        // SETUP ====================================
        $routeParams = [];
        if (!is_null($connectionId)) {
            $routeParams['_route_params']['connectionId'] = $connectionId;
        }
        $request = new Request(
            [],
            [],
            $routeParams,
        );

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->connectionCreateRequestParser->parse($request);

        // EVALUATION ====================================
    }
}