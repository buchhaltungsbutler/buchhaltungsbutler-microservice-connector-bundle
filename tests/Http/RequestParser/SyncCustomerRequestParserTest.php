<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\SyncCustomerRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\SyncCustomerRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\SyncCustomerRequestParser
 */
final class SyncCustomerRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->syncCustomerRequestParser = new SyncCustomerRequestParser();
    }

    public function testSupportsAccountGetByConnectionIdRequestSuccessful(): void
    {
        $this->assertTrue($this->syncCustomerRequestParser->supports(SyncCustomerRequest::class));
    }

    private function dataProviderParseSyncCustomerRequestSuccessful(): array
    {
        return [
            'null limit' => ['customerId'],
        ];
    }

    /**
     * @dataProvider dataProviderParseSyncCustomerRequestSuccessful
     */
    public function testParseSyncCustomerRequestSuccessful(string $customerId): void
    {
        // SETUP ====================================
        $request = new Request([], [], ['_route_params' => ['customerId' => $customerId]]);

        // EXECUTION ====================================
        $SyncCustomerRequest = $this->syncCustomerRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($customerId, $SyncCustomerRequest->getCustomerId());
    }

    private function dataProviderParseSyncCustomerRequestUnsuccessful(): array
    {
        return [
            'null customerId' => [null],
            'empty customerId' => [''],
            'wrong type customerId' => [false],
        ];
    }

    /**
     * @dataProvider dataProviderParseSyncCustomerRequestUnsuccessful
     */
    public function testParseSyncCustomerRequestUnsuccessful($connectionId): void
    {
        // SETUP ====================================
        $request = new Request([], [], ['_route_params' => ['customerId' => $connectionId]]);

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->syncCustomerRequestParser->parse($request);

        // EVALUATION ====================================
    }
}
