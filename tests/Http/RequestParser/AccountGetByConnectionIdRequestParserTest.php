<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountGetByConnectionIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\AccountGetByConnectionIdRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\AccountGetByConnectionIdRequestParser
 */
final class AccountGetByConnectionIdRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->accountGetByConnectionIdRequestParser = new AccountGetByConnectionIdRequestParser();
    }

    public function testSupportsAccountGetByConnectionIdRequestSuccessful(): void
    {
        $this->assertTrue($this->accountGetByConnectionIdRequestParser->supports(AccountGetByConnectionIdRequest::class));
    }

    private function dataProviderParseAccountGetByConnectionIdRequestSuccessful(): array
    {
        return [
            'null limit' => ['1', null],
            'limit' => ['1', '1'],
        ];
    }

    /**
     * @dataProvider dataProviderParseAccountGetByConnectionIdRequestSuccessful
     */
    public function testParseAccountGetByConnectionIdRequestSuccessful(string $connectionId, ?string $limit): void
    {
        // SETUP ====================================
        $request = new Request(['limit' => $limit], [], ['_route_params' => ['connectionId' => $connectionId]]);

        // EXECUTION ====================================
        $accountGetByConnectionIdRequest = $this->accountGetByConnectionIdRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($connectionId, $accountGetByConnectionIdRequest->getConnectionId());
        $this->assertEquals(empty($limit) ? null : (int) $limit, $accountGetByConnectionIdRequest->getLimit());
    }

    private function dataProviderParseAccountGetByConnectionIdRequestUnsuccessful(): array
    {
        return [
            'empty connectionId' => ['', null],
            'non numeric connectionId' => ['error', null],
            'wrong type connectionId' => [false, null],
            'empty limit' => ['1', ''],
            'non numeric limit' => ['1', 'error'],
            'wrong type limit' => ['1', false],
        ];
    }

    /**
     * @dataProvider dataProviderParseAccountGetByConnectionIdRequestUnsuccessful
     */
    public function testParseAccountGetByConnectionIdRequestUnsuccessful($connectionId, $limit): void
    {
        // SETUP ====================================
        $request = new Request(['limit' => $limit], [], ['_route_params' => ['connectionId' => $connectionId]]);

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->accountGetByConnectionIdRequestParser->parse($request);

        // EVALUATION ====================================
    }
}
