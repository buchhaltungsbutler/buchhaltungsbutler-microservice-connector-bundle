<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\Controller;

use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\RequestParserException\RequestParameterException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountGetByCustomerIdRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\AccountGetByCustomerIdRequestParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \BuchhaltungsButler\MicroserviceConnectorBundle\Http\RequestParser\AccountGetByCustomerIdRequestParser
 */
final class AccountGetByCustomerIdRequestParserTest extends KernelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // boot kernel (KernelTestCase) or create client (WebTestCase) before fixture handling
        self::bootKernel();
        $this->accountGetByCustomerIdRequestParser = new AccountGetByCustomerIdRequestParser();
    }

    public function testSupportsAccountGetByConnectionIdRequestSuccessful(): void
    {
        $this->assertTrue($this->accountGetByCustomerIdRequestParser->supports(AccountGetByCustomerIdRequest::class));
    }

    private function dataProviderParseAccountGetByCustomerIdRequestSuccessful(): array
    {
        return [
            'null limit' => ['customerId', null],
            'limit' => ['customerId', '1'],
        ];
    }

    /**
     * @dataProvider dataProviderParseAccountGetByCustomerIdRequestSuccessful
     */
    public function testParseAccountGetByCustomerIdRequestSuccessful(string $customerId, ?string $limit): void
    {
        // SETUP ====================================
        $request = new Request(['limit' => $limit], [], ['_route_params' => ['customerId' => $customerId]]);

        // EXECUTION ====================================
        $accountGetByConnectionIdRequest = $this->accountGetByCustomerIdRequestParser->parse($request);

        // EVALUATION ====================================
        $this->assertEquals($customerId, $accountGetByConnectionIdRequest->getCustomerId());
        $this->assertEquals(empty($limit) ? null : (int) $limit, $accountGetByConnectionIdRequest->getLimit());
    }

    private function dataProviderParseAccountGetByCustomerIdRequestUnsuccessful(): array
    {
        return [
            'empty customerId' => ['', null],
            'empty limit' => ['customerId', ''],
            'non numeric limit' => ['customerId', 'error'],
        ];
    }

    /**
     * @dataProvider dataProviderParseAccountGetByCustomerIdRequestUnsuccessful
     */
    public function testParseAccountGetByCustomerIdRequestUnsuccessful($connectionId, $limit): void
    {
        // SETUP ====================================
        $request = new Request(['limit' => $limit], [], ['_route_params' => ['customerId' => $connectionId]]);

        // EXECUTION ====================================
        $this->expectException(RequestParameterException::class);
        $this->accountGetByCustomerIdRequestParser->parse($request);

        // EVALUATION ====================================
    }
}
