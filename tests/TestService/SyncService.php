<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\TestService;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Customer;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\SyncCustomerRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\SyncCustomerResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\SyncInterface;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\CustomerPersister;

class SyncService extends SyncInterface
{
    public function __construct(
        private CustomerPersister $customerPersister
    )
    {
    }

    public function syncCustomer(SyncCustomerRequest $request): SyncCustomerResponse
    {
        $customer = $this->customerPersister->getCustomerByCustomerId($request->getCustomerId());

        if (!$customer instanceof Customer) {
            throw new CustomerNotFoundException('customer not found with id: '. $request->getCustomerId());
        }

        // TODO: initiate sync job

        return new SyncCustomerResponse($request->getCustomerId());
    }
}
