<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\TestService;

use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\OAuthGetUrlRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\OAuthGetUrlResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\OAuthInterface;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\RequestIdService;

class OAuthService extends OAuthInterface
{
    public function __construct(private RequestIdService $requestIdService)
    {
    }

    public function getOAuthUrl(OAuthGetUrlRequest $request): OAuthGetUrlResponse
    {
        $url = 'https://oauth.test.com'
            .'&redirect_uri=' . urlencode($request->getRuName())
            .'&state=' . urlencode($request->getCustomerId()
                . '|' . $request->getConnectionId()
                . '|' . $this->requestIdService->getRequestId()
                . '|' . $request->getParameters()['name'] ?? '');

        return new OAuthGetUrlResponse($url);
    }
}
