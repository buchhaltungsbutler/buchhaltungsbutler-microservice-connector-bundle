<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\TestService;


use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Connection;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\AccountNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\ConnectionNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\AccountUpdateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\AccountUpdateResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\AccountInterface;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\AccountPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\ConnectionPersister;

class AccountService extends AccountInterface
{
    public function __construct(
        AccountPersister $accountPersister,
        private ConnectionPersister $connectionPersister
    )
    {
        parent::__construct($accountPersister);
    }

    public function updateAccount(AccountUpdateRequest $request): AccountUpdateResponse
    {
        $accountEntity = $this->accountPersister->getAccountById($request->getAccountId());

        if (!$accountEntity) {
            throw new AccountNotFoundException('account not found for id '. $request->getAccountId());
        }

        $connection = $this->connectionPersister->find($request->getConnectionId());
        if (!$connection instanceof Connection) {
            throw new ConnectionNotFoundException('connection not found for id '. $request->getConnectionId());
        }

        // do not update account name/description
//        $accountEntity->setDescription($request->getDescription());
        $accountEntity->setImportSince($request->getImportSince());
        $accountEntity->setConnection($connection);

        $this->accountPersister->persist($accountEntity);

        return new AccountUpdateResponse($accountEntity->getId());
    }
}
