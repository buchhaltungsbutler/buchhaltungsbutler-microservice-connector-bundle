<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\TestService;


use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\InfoResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\InfoInterface;

class InfoService extends InfoInterface
{
    public function getInfo(): InfoResponse
    {
        return new InfoResponse([
            'title' => 'BHB Test Microservice',
            'id' => 'test',
            'authMethod' => ['oauth2'],
            'apiVersion' => 1.0,
            'apiFeatures' => ['transactions']
        ]);
    }
}
