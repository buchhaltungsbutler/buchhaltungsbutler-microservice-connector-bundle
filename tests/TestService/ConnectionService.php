<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests\TestService;



use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Account;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Connection;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Customer;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\ConnectionNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Exception\EntityNotFoundException\CustomerNotFoundException;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionCreateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionDeleteRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Request\ConnectionUpdateRequest;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\ConnectionCreateResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\ConnectionDeleteResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\Http\Response\ConnectionUpdateResponse;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicInterfaces\ConnectionInterface;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\AccountPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\ConnectionPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\CustomerPersister;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\DatabaseTimeFormatter;

class ConnectionService extends ConnectionInterface
{
    /**
     * @param ConnectionPersister $connectionPersister
     * @param CustomerPersister $customerPersister
     * @param AccountPersister $accountPersister
     */
    public function __construct(
        ConnectionPersister                             $connectionPersister,
        CustomerPersister                               $customerPersister,
        private readonly AccountPersister               $accountPersister,
    )
    {
        parent::__construct($connectionPersister, $customerPersister);
    }

    /**
     * @param ConnectionCreateRequest $request
     * @return ConnectionCreateResponse
     * @throws \Exception
     */
    public function createConnection(ConnectionCreateRequest $request): ConnectionCreateResponse
    {
        $customer = $this->customerPersister->getCustomerByCustomerId($request->getCustomerId());

        if (is_null($customer)) {
            $customer = new Customer(0, $request->getCustomerId(), new \DateTimeImmutable());
            $this->customerPersister->persist($customer, false);
        }

        $parameters = $request->getParameters();
        $now = new \DateTimeImmutable();

        $connectionEntity = new Connection(
            0,
            $customer,
            $request->getDescription(),
            ($parameters['production'] ?? false) === 'true',
            $parameters['market'] ?? 'DE',
            ($parameters['broken'] ?? false) === 'true',
            $parameters['metaData'] ?? [],
            '',
            $now,
            '',
            $now,
            $now,
            $now,
        );

        $this->connectionPersister->persist($connectionEntity, false);

        $account = new Account(
            0,
            $connectionEntity,
            $connectionEntity->getDescription(). ' - TestAccount',
            ['backup' => true],
            null,
            null,
            null,
            $now,
            $now
        );

        $this->accountPersister->persist($account);

        return new ConnectionCreateResponse($connectionEntity->getId());
    }

    /**
     * @param ConnectionUpdateRequest $request
     * @return ConnectionUpdateResponse
     * @throws \Exception
     */
    public function updateConnection(ConnectionUpdateRequest $request): ConnectionUpdateResponse
    {
        $connectionEntity = $this->connectionPersister->getConnectionById($request->getConnectionId());

        if (is_null($connectionEntity)) {
            throw new ConnectionNotFoundException('connection not found for id: '. $request->getConnectionId());
        }

        $customer = $this->customerPersister->getCustomerByCustomerId($request->getCustomerId());

        if (is_null($customer)) {
            throw new CustomerNotFoundException(__METHOD__. ': customer with id ('.$request->getCustomerId().')  not found');
        }

        $parameters = $request->getParameters();

        $connectionEntity->setCustomer($customer);
        $connectionEntity->setDescription($request->getDescription());
        $connectionEntity->setProduction($parameters['production']);
        $connectionEntity->setMarket($parameters['market']);
        $connectionEntity->setBroken($parameters['broken']);
        $connectionEntity->setMetaData($parameters['metaData'] ?? []);
        $connectionEntity->setAccessToken($parameters['accessToken']);
        $connectionEntity->setAccessExpireTime(DatabaseTimeFormatter::stringToDateTimeImmutable($parameters['accessTokenExpireTime']));
        $connectionEntity->setRefreshToken($parameters['refreshToken']);
        $connectionEntity->setRefreshExpireTime(DatabaseTimeFormatter::stringToDateTimeImmutable($parameters['refreshTokenExpireTime']));
        $connectionEntity->setUpdatedAt(new \DateTimeImmutable());

        $this->connectionPersister->persist($connectionEntity);

        return new ConnectionUpdateResponse($connectionEntity->getId());
    }

    /**
     * @param ConnectionDeleteRequest $request
     * @return ConnectionDeleteResponse
     * @throws ConnectionNotFoundException
     */
    public function deleteConnection(ConnectionDeleteRequest $request): ConnectionDeleteResponse
    {
        $this->connectionPersister->removeById($request->getConnectionId());

        return new ConnectionDeleteResponse($request->getConnectionId());
    }
}
