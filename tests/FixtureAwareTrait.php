<?php
declare(strict_types = 1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Tests;

use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Account;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Connection;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Customer;
use BuchhaltungsButler\MicroserviceConnectorBundle\Entity\Transaction;
use BuchhaltungsButler\MicroserviceConnectorBundle\PublicService\EntityConverterService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\ToolsException;

/**
 * a trait that introduces fixture loading into test cases
 * can only be used with symfony KernelTestCase class or derivatives
 */
trait FixtureAwareTrait
{
    /**
     * @var SchemaTool[]
     */
    private array $schemaTool;

    /**
     * @var ClassMetadata[][]
     */
    private array $metaData;

    /**
     * @var EntityManager[]
     */
    private array $entityManagers;
    
    public string $testTime = '2022-01-01 00:00:00';

    /**
     * @param string $managerName
     * @return EntityManager
     */
    public function getEntityManager(string $managerName): EntityManager
    {
        if (!isset($this->entityManagers[$managerName])) {
            $that = get_called_class();
            $this->entityManagers[$managerName] = $that::$kernel->getContainer()->get('doctrine')->getManager($managerName);
        }

        return $this->entityManagers[$managerName];
    }

    /**
     * @param string $managerName
     * @return void
     * @throws ToolsException
     */
    public function cleanDB(string $managerName): void
    {
        if (!isset($this->schemaTool[$managerName]) || !($this->metaData[$managerName] ?? false)) {
            $entityManager = $this->getEntityManager($managerName);
            $this->metaData[$managerName] = $entityManager->getMetadataFactory()->getAllMetadata();
            $this->schemaTool[$managerName] = new SchemaTool($entityManager);
        }

        // Drop and recreate tables for all entities
        $this->schemaTool[$managerName]->dropSchema($this->metaData[$managerName]);
        $this->schemaTool[$managerName]->createSchema($this->metaData[$managerName]);
    }

    /**
     * @param array $template
     * @param array $customData
     * @param string $entityClass
     * @param string $managerName
     * @return array
     * @throws \Doctrine\ORM\Exception\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ReflectionException
     */
    private function given(
        array $template,
        array $customData,
        string $entityClass,
        string $managerName
    ): array
    {
        $data = array_map(
            function (array $customDatum) use ($template) {
                return array_merge($template, $customDatum);
            },
            $customData
        );

        $entityManager = $this->getEntityManager($managerName);
        $entities = [];

        foreach ($data as $datum) {
            $entity = EntityConverterService::fromArray($entityClass, $datum);
            $entityManager->persist($entity);
            $entities[] = $entity;
        }

        $entityManager->flush();

        return $entities;
    }

    protected function givenConnection(array ...$customData): array
    {
        $template = [
            'id' => 0,
            'customer' => null,
            'description' => 'testDescription',
            'production' => true,
            'market' => 'DE',
            'broken' => false,
            'metaData' => [],
            'accessToken' => 'testAccessToken',
            'accessExpireTime' => new \DateTimeImmutable($this->testTime),
            'refreshToken' => 'testRefreshToken',
            'refreshExpireTime' => new \DateTimeImmutable($this->testTime),
            'updatedAt' => new \DateTimeImmutable($this->testTime),
            'createdAt' => new \DateTimeImmutable($this->testTime),
        ];

        $managerName = 'microservice_connector_bundle';

        return $this->given($template, $customData, Connection::class, $managerName);
    }

    protected function givenTransaction(array ...$customData): array
    {
        $template = [
            'id' => 0,
            'account' => null,
            'amount' => 1.1,
            'currency' => 'EUR',
            'feeAmount' => -1.0,
            'sourceTransactionId' => 'testSourceTransactionId',
            'purpose' => 'testPurpose',
            'paymentReference' => 'testPaymentReference',
            'type' => 'testType',
            'counterParty' => 'testCounterParty',
            'bookingText' => 'testBookingText',
            'bookedAt' => new \DateTimeImmutable($this->testTime),
            'valuedAt' => new \DateTimeImmutable($this->testTime),
            'createdAt' => new \DateTimeImmutable($this->testTime),
            'deleted' => false,
        ];

        $managerName = 'microservice_connector_bundle';

        return $this->given($template, $customData, Transaction::class, $managerName);
    }

    protected function givenAccount(array ...$customData): array
    {
        $template = [
            'id' => 0,
            'connection' => null,
            'description' => 'testDescription',
            'metaData' => [],
            'importSince' => new \DateTimeImmutable($this->testTime),
            'lastSync' => new \DateTimeImmutable($this->testTime),
            'lastSyncSuccessful' => new \DateTimeImmutable($this->testTime),
            'updatedAt' => new \DateTimeImmutable($this->testTime),
            'createdAt' => new \DateTimeImmutable($this->testTime),
        ];

        $managerName = 'microservice_connector_bundle';

        return $this->given($template, $customData, Account::class, $managerName);
    }
    protected function givenCustomer(array ...$customData): array
    {
        $template = [
            'id' => 0,
            'customerId' => 'testCustomerId',
            'createdAt' => new \DateTimeImmutable($this->testTime),
        ];

        $managerName = 'microservice_connector_bundle';

        return $this->given($template, $customData, Customer::class, $managerName);
    }
}