# create databases
CREATE DATABASE IF NOT EXISTS `bhb_external_data_provider_bundle_data_test`;

# create users and grant rights
DROP USER IF EXISTS 'root';
CREATE USER 'root'@'%' IDENTIFIED BY 'bhbb132';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';

DROP USER IF EXISTS 'bb';
CREATE USER 'bb'@'%' IDENTIFIED BY 'bb';
GRANT ALL PRIVILEGES ON `bhb_external_data_provider_bundle_data_test`.* TO 'bb'@'%';


