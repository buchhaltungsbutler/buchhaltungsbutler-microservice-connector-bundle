<?php

declare(strict_types=1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220727132816 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account ADD meta_data LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', DROP active, CHANGE description description VARCHAR(254) NOT NULL');
        $this->addSql('ALTER TABLE connection ADD meta_data LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `account` ADD active TINYINT(1) NOT NULL, DROP meta_data, CHANGE description description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE `connection` DROP meta_data');
    }
}
