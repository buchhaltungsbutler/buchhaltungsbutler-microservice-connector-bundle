<?php

declare(strict_types=1);

namespace BHB\ExternDataProviderServiceBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220719144517 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account DROP FOREIGN KEY FK_7D3656A4699A4AC3');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A4699A4AC3 FOREIGN KEY (fk_connection_id) REFERENCES `connection` (pk) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE connection DROP FOREIGN KEY FK_29F77366FA636FC7');
        $this->addSql('ALTER TABLE connection ADD CONSTRAINT FK_29F77366FA636FC7 FOREIGN KEY (fk_customer_id) REFERENCES `customer` (pk) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D16B059529');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D16B059529 FOREIGN KEY (fk_account_id) REFERENCES `account` (pk) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `account` DROP FOREIGN KEY FK_7D3656A4699A4AC3');
        $this->addSql('ALTER TABLE `account` ADD CONSTRAINT FK_7D3656A4699A4AC3 FOREIGN KEY (fk_connection_id) REFERENCES connection (pk)');
        $this->addSql('ALTER TABLE `connection` DROP FOREIGN KEY FK_29F77366FA636FC7');
        $this->addSql('ALTER TABLE `connection` ADD CONSTRAINT FK_29F77366FA636FC7 FOREIGN KEY (fk_customer_id) REFERENCES customer (pk)');
        $this->addSql('ALTER TABLE `transaction` DROP FOREIGN KEY FK_723705D16B059529');
        $this->addSql('ALTER TABLE `transaction` ADD CONSTRAINT FK_723705D16B059529 FOREIGN KEY (fk_account_id) REFERENCES account (pk)');
    }
}
