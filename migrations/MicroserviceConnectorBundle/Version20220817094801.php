<?php

declare(strict_types=1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220817094801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer CHANGE customer_id customer_id VARCHAR(190) NOT NULL');
        $this->addSql('ALTER TABLE transaction CHANGE source_transaction_id source_transaction_id VARCHAR(190) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `transaction` CHANGE source_transaction_id source_transaction_id VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE `customer` CHANGE customer_id customer_id VARCHAR(255) NOT NULL');
    }
}
