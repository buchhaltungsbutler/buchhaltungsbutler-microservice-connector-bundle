<?php

declare(strict_types=1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220714103335 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `account` (pk INT AUTO_INCREMENT NOT NULL, fk_connection_id INT NOT NULL, description VARCHAR(255) NOT NULL, import_since DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', last_sync DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', last_sync_successful DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7D3656A4699A4AC3 (fk_connection_id), PRIMARY KEY(pk)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `connection` (pk INT AUTO_INCREMENT NOT NULL, fk_customer_id INT NOT NULL, description VARCHAR(255) NOT NULL, production TINYINT(1) NOT NULL, market VARCHAR(3) NOT NULL, broken TINYINT(1) NOT NULL, access_token VARCHAR(4096) NOT NULL, access_expire_time DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', refresh_token VARCHAR(4096) NOT NULL, refresh_expire_time DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_29F77366FA636FC7 (fk_customer_id), PRIMARY KEY(pk)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `customer` (pk INT AUTO_INCREMENT NOT NULL, customer_id VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(pk)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `transaction` (pk INT AUTO_INCREMENT NOT NULL, fk_account_id INT NOT NULL, amount NUMERIC(10, 2) NOT NULL, currency VARCHAR(3) NOT NULL, purpose LONGTEXT NOT NULL, payment_reference VARCHAR(254) NOT NULL, type VARCHAR(254) NOT NULL, counter_party LONGTEXT NOT NULL, booking_text LONGTEXT NOT NULL, booked_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', valued_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', source_transaction_id VARCHAR(190) NOT NULL, INDEX IDX_723705D16B059529 (fk_account_id), PRIMARY KEY(pk)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `account` ADD CONSTRAINT FK_7D3656A4699A4AC3 FOREIGN KEY (fk_connection_id) REFERENCES `connection` (pk)');
        $this->addSql('ALTER TABLE `connection` ADD CONSTRAINT FK_29F77366FA636FC7 FOREIGN KEY (fk_customer_id) REFERENCES `customer` (pk)');
        $this->addSql('ALTER TABLE `transaction` ADD CONSTRAINT FK_723705D16B059529 FOREIGN KEY (fk_account_id) REFERENCES `account` (pk)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `transaction` DROP FOREIGN KEY FK_723705D16B059529');
        $this->addSql('ALTER TABLE `account` DROP FOREIGN KEY FK_7D3656A4699A4AC3');
        $this->addSql('ALTER TABLE `connection` DROP FOREIGN KEY FK_29F77366FA636FC7');
        $this->addSql('DROP TABLE `account`');
        $this->addSql('DROP TABLE `connection`');
        $this->addSql('DROP TABLE `customer`');
        $this->addSql('DROP TABLE `transaction`');
    }
}
