<?php

declare(strict_types=1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220816135205 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX customer_id ON customer (customer_id)');
        $this->addSql('CREATE UNIQUE INDEX source_transaction_id ON transaction (source_transaction_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX source_transaction_id ON `transaction`');
        $this->addSql('DROP INDEX customer_id ON `customer`');
    }
}
