<?php

declare(strict_types=1);

namespace BuchhaltungsButler\MicroserviceConnectorBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221108175601 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX fk_account_id_source_transaction_id ON transaction');
        $this->addSql('ALTER TABLE transaction ADD deleted TINYINT(1) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX fk_account_id_source_transaction_id ON transaction (fk_account_id, source_transaction_id, deleted)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX fk_account_id_source_transaction_id ON `transaction`');
        $this->addSql('ALTER TABLE `transaction` DROP deleted');
        $this->addSql('CREATE UNIQUE INDEX fk_account_id_source_transaction_id ON `transaction` (fk_account_id, source_transaction_id)');
    }
}
